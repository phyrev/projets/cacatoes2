import pandas as pd
from ipyleaflet import Map, GeoJSON, Marker, CircleMarker, DrawControl, ScaleControl, Rectangle, DivIcon
import ipywidgets as ipyw
from pandas.io.json import json_normalize
import CacatoesUtils
import PluvioSahelUtils
from ipywidgets import HTML
import proplot as pplot
import numpy as np
import datetime
import scipy.stats
import glob

def buildValuesForBarJirak(serie, percent=False):
    factor = 1.0
    if percent==True:
        factor = 100.0
    val0 = factor * len(serie.where(serie == 0).dropna())/len(serie)
    val1 = factor * len(serie.where(serie == 1).dropna())/len(serie)
    val2 = factor * len(serie.where(serie == 2).dropna())/len(serie)
    val3 = factor * len(serie.where(serie == 3).dropna())/len(serie)
    val4 = factor * len(serie.where(serie == 4).dropna())/len(serie)
    val = [val0, val1, val2, val3, val4]
    return val


def GetAllMCSDataFromCacatoes(startYear, endYear, startMonth, endMonth, cell, variablesDictionary, lonpathFilesCacatoes = '../CACATOES/'):    
    lonMin = cell[0][0]
    lonMax = cell[0][1]
    latMin = cell[1][0]
    latMax = cell[1][1]
    start = True
    nbYear = endYear - startYear + 1
    nbMonth = endMonth - startMonth + 1
    for y in np.arange(nbYear):
        year = y+startYear
        for m in np.arange(nbMonth):
            month = m+startMonth
            dirFiles = '../CACATOES/AMMA-CATCH/' + str(year) + '/'
            fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(month).zfill(2)+'01'+'*'))
            
            if len(fileCACATOES) > 1:
                raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
             
            fileMonth = fileCACATOES[0]
            cacatoesValues = CacatoesUtils.GET_CACATOES_EUMETSAT_FOR_VARIABLES(fileMonth,lonMin,lonMax,latMin,latMax,variablesDictionary)
            #resMonth = CacatoesUtils.open_CACATOES_EUMETSAT(fileMonth,lonMin,lonMax,latMin,latMax)
            shape = np.shape(cacatoesValues['QCmcs_Label'])
            
            nbDays = shape[0]
            nbMCSPerDay = shape[1]
            nbLonStep = shape[2]
            nbLatStep = shape[3]
            listMCS = []
            listDay = []
            tabVar = []#liste de listes
            for key in variablesDictionary:
                varName = variablesDictionary[key][0]
                if varName == 'QCmcs_Label':
                    continue                
                tabVar.append([])
                
            for d in np.arange(nbDays):
                day = pd.Timestamp(year=year, month=month, day=d+1)

                for m in np.arange(nbMCSPerDay):
                    for lo in np.arange(nbLonStep):
                        for la in np.arange(nbLatStep):
                            mcs = cacatoesValues['QCmcs_Label'][d,m,lo,la]
                            if mcs > 0:
                                listMCS.append(mcs)
                                listDay.append(day)
                                for key in variablesDictionary:
                                    varName = variablesDictionary[key][0]
                                    if varName == 'QCmcs_Label':
                                        continue
                                    
                                    value = cacatoesValues[varName][d,m,lo,la]                                   
                                    tabVar[key-1].append(value)                              
                        
            arrayMCS, index = np.unique(listMCS, return_index=True)
            arrayDay = np.asarray(listDay)[index]
            d = {}
            d[variablesDictionary[0][1]] = arrayMCS
            d['Day'] = arrayDay
            for key in variablesDictionary:
                varName = variablesDictionary[key][0]
                if varName == 'QCmcs_Label':
                    continue                
                arrayVar = np.asarray(tabVar[key-1])[index]
                d[variablesDictionary[key][1]] = arrayVar
            
            df = pd.DataFrame(data=d)
            df = df.sort_values(by=['Day'])
            df.index = df['Day']
            df = df.drop(['Day'], axis=1)
            
            if start==True:
                dfGlobal = df
                start = False
                
            else:
                dfGlobal = pd.concat([dfGlobal, df],axis=0)
    dfGlobal = AjustTbavg208K_With_TempMini(dfGlobal)            
    return dfGlobal
    
def AjustTbavg208K_With_TempMini(df):
    pd.set_option('mode.chained_assignment',None)
    if 'Temp < 208' in df.columns and 'TempMini' in df.columns :
        temp208 = df['Temp < 208']
        tempMin = df['TempMini']
        for i in np.arange(len(temp208)):
            value = temp208.iloc[i]
            if value < 0.1:
                value = np.maximum(208.0, tempMin[i])
                temp208.iloc[i] = value
        df = df.drop('TempMini', axis=1)
    else:
        print('echec!')
    return df
    
def BuildNbMCSPerYearTab(df):
    dfYear = {}
    sizeDf = len(df.index)
    minYear = df.index[0].year
    maxYear = df.index[sizeDf - 1].year
    nbYear = maxYear - minYear + 1
    nbMCSYear = np.zeros(nbYear)
    xYearInGraph = np.arange(minYear,maxYear+1,1)
    for y in np.arange(nbYear):
        year = y+minYear
        before = year-1
        after = year+1
        indexYear = np.intersect1d(np.where(df.index.year > before),np.where(df.index.year < after))
        dfYear[year] = df.iloc[indexYear]
        nbMCSYear[y] = len(dfYear[year])
    return xYearInGraph, nbMCSYear
    
def BuildSubDataFrameForPeriod(dfGlobal, nbPeriod):
    sizeDf = len(dfGlobal.index)
    minYear = dfGlobal.index[0].year
    maxYear = dfGlobal.index[sizeDf - 1].year
    nbYear = maxYear - minYear + 1
    lenPeriod = (int) (nbYear / nbPeriod)
    startPeriods = np.zeros(nbPeriod)
    endPeriods = np.zeros(nbPeriod)
    for i in np.arange(nbPeriod):
        startPeriods[i] = minYear + i * lenPeriod
        endPeriods[i] = minYear + (i+1) * lenPeriod - 1
        if i == 0:
            subDf = dfGlobal.iloc[np.where(dfGlobal.index.year < startPeriods[i])[0]]
    subDfDict = {}
    subDfDict[0] = dfGlobal.iloc[np.where(dfGlobal.index.year < startPeriods[1])[0]]
    for i in np.arange(1,nbPeriod-1,1):
        inter = np.intersect1d(np.where(dfGlobal.index.year > endPeriods[i-1]),np.where(dfGlobal.index.year < startPeriods[i+1]))       
        subDfDict[i] = dfGlobal.iloc[inter]
    subDfDict[nbPeriod-1] = dfGlobal.iloc[np.where(dfGlobal.index.year > endPeriods[nbPeriod-2])[0]]
    return subDfDict, startPeriods, endPeriods
    
def PlotNbMCSTendance(dfGlobal, nbPeriod):
    xYear, nbMCSYear = BuildNbMCSPerYearTab(dfGlobal)
    subDfDict, startPeriods, endPeriods = BuildSubDataFrameForPeriod(dfGlobal, nbPeriod)
    barLabel = []
    indiceValues = []
    xForLine = []
    nbYearInPeriod = endPeriods[0] - startPeriods[0] + 1
    nbYearTotal = int(nbYearInPeriod * nbPeriod)
    for i in np.arange(nbPeriod):
        barLabel.append(str(int(startPeriods[i])) + ' --> ' + str(int(endPeriods[i])))
        xForLine.append(i)
        if i == 0:
            indiceValues.append(100)
        else:
            indiceValues.append(100 * len(subDfDict[i])/len(subDfDict[0]))
    Ylim = [80, 1.1 * np.max(indiceValues)]
    pplot.rc.abc = 'a.'
    pplot.rc.titleloc = 'l'
    pplot.rc['legend.fontsize'] = 'medium'
    gs = pplot.GridSpec(nrows=1, ncols = 2, wratios=(2, 1))

    fig = pplot.figure(refaspect=2, refwidth=5, share=False, suptitle='Nombre de MCS : Evolution décennale')
    ax2 = fig.subplot(gs[0,0],title='Valeurs annuelles',ylabel='Nb de MCS',xlabel='Années')
    titleAx = 'Par période de ' + str(int(nbYearInPeriod)) + ' ans'
    ax = fig.subplot(gs[0,1], title=titleAx)
    
    labelForBars = "Indice nombre de MCS\n(100 <=> période " + barLabel[0]
    obj = ax.bar(barLabel, indiceValues, label=labelForBars, width=0.5, color='grey5')
    ax.line(xForLine, indiceValues, marker='o', color = 'black')
    ax.format(xlocator=1, xminorlocator=0.5, xticklen=0, ytickminor=False,ylim=Ylim)
    ax.legend(ncols=1, loc='ur')

    yLimNbMCS=[np.min(nbMCSYear),np.max(nbMCSYear)]
    ax2.line(xYear, nbMCSYear, marker='o', color = 'black', label='Nombre total MCS par an')
    mobile = np.convolve(nbMCSYear, [1, 1, 1,1,1], 'valid') / 5
    maxPlotXForMobile = nbYearTotal-2
    ax2.line(xYear[2:maxPlotXForMobile], mobile, color = 'red', label='Moyenne mobile sur 5 ans')
    ax2.format(ytickminor=False,ylim=yLimNbMCS)
    ax2.legend(ncols=1, loc='lr')
    fileName = 'EvolutionDecennale_MCS_NbTotal.jpg'
    fig.savefig(fileName)
    
def PlotNbMCSVariablesTendance(dfGlobal, nbPeriod,variablesDictionary, percentile_to_use):
    subDfDict, startPeriods, endPeriods = BuildSubDataFrameForPeriod(dfGlobal, nbPeriod)
    
    barLabel = []
    nbYearInPeriod = endPeriods[0] - startPeriods[0] + 1
    nbYearTotal = int(nbYearInPeriod * nbPeriod)

    for i in np.arange(nbPeriod):
        barLabel.append(str(int(startPeriods[i])) + ' --> ' + str(int(endPeriods[i])))
    colors = []
    for key in variablesDictionary:
        if key == 0:
            continue
        if key > 6:
            break
        if (key-1)%5 == 0:
            colors.append(['red2','red7'])
        elif (key-1)%5 == 1:
            colors.append(['green2','green7'])
        elif (key-1)%5 == 2:
            colors.append(['blue2','blue7'])
        elif (key-1)%5 == 3:
            colors.append(['orange2','orange7'])
        elif (key-1)%5 == 4:
            colors.append(['violet2','violet7'])

    pplot.rc.abc = 'a.'
    pplot.rc.titleloc = 'l'
    pplot.rc['legend.fontsize'] = 'medium'
    nbVariablesToPlot = len(variablesDictionary) - 2
    if nbVariablesToPlot%2 == 0:
        nbRows = int(nbVariablesToPlot/2)
    else:
        nbRows = int(np.ceil(nbVariablesToPlot/2))
    gs = pplot.GridSpec(nrows=nbRows, ncols = 2)
    Suptitle = 'Evolution décennale de ' + str(int(nbVariablesToPlot)) + ' variables'
    fig = pplot.figure(refaspect=2, refwidth=4.8, share=False, suptitle=Suptitle)
    k = 0
    for key in variablesDictionary:
        if key == 0:
            continue
        if key > nbVariablesToPlot:
            break
        serieToTest = dfGlobal[variablesDictionary[key][1]]
        
        if variablesDictionary[key][1] != 'Jirak':        
            if variablesDictionary[key][1] == 'Temp < 208':
                minpercentile = 100 - percentile_to_use
                valuePercentile = np.percentile(serieToTest,minpercentile)
                indicesExtrems = []
                for i in np.arange(nbPeriod):
                    indicesExtrems.append(np.where(subDfDict[i][variablesDictionary[key][1]] < valuePercentile)[0])
            elif variablesDictionary[key][1] == 'Excentricite':
                serieToTest = dfGlobal['Excentricite'][np.where(dfGlobal['Excentricite'] > 0)[0]]
                subExcen_no0, startPeriods, endPeriods = BuildSubDataFrameForPeriod(serieToTest, nbPeriod)
                valuePercentile = np.percentile(serieToTest,percentile_to_use)
                indicesExtrems = []
                for i in np.arange(nbPeriod):
                    indicesExtrems.append(np.where(subExcen_no0[i] > valuePercentile)[0])
            else:
                valuePercentile = np.percentile(serieToTest,percentile_to_use)
                
                indicesExtrems = []
                for i in np.arange(nbPeriod):
                    indicesExtrems.append(np.where(subDfDict[i][variablesDictionary[key][1]] > valuePercentile)[0])                
            p = []
            g = []
            r = []
            for i in np.arange(nbPeriod):
                p.append(len(indicesExtrems[i])/len(subDfDict[i]))
                g.append(100 * len(subDfDict[i]) / len(subDfDict[0]))
            
            for i in np.arange(nbPeriod):
                r.append(g[i] * p[i] / p[0])
            general = g
            extrem = r
        
        i = int(np.floor(k/2))
        j = k%2
        if variablesDictionary[key][1] == 'Temp < 208':
            Title = variablesDictionary[key][2] + ' < perc. ' + str(minpercentile) + ' (' + str(int(np.around(valuePercentile,0))) + ' ' + variablesDictionary[key][3] + ')'
        elif variablesDictionary[key][1] == 'Excentricite':
            Title = variablesDictionary[key][2] + ' > perc. ' + str(percentile_to_use) + ' (' + str(np.around(valuePercentile,2)) + ' ' + variablesDictionary[key][3] + ')'
        else:
            Title = variablesDictionary[key][2] + ' > perc. ' + str(percentile_to_use) + ' (' + str(int(np.around(valuePercentile,0))) + ' ' + variablesDictionary[key][3] + ')'
        ax = fig.subplot(gs[i,j], title=Title)
        if variablesDictionary[key][1] == 'Jirak':
            bins = ['']
            bins.extend(barLabel)
            x = np.arange(nbPeriod)
            legendObj = []
            indicesForExtrem = []
            Jirak = []
            JirakExtrem = []
            for i in np.arange(nbPeriod):
                indicesForExtrem.append(np.where(subDfDict[i][variablesDictionary[1][1]] > 70000)[0])
            
            for i in np.arange(nbPeriod):
                Jirak.append(buildValuesForBarJirak(subDfDict[i][variablesDictionary[key][1]],False))
                JirakExtrem.append(buildValuesForBarJirak(subDfDict[i][variablesDictionary[key][1]][indicesForExtrem[i]],False))
            
            Jirak_MCC = []
            Jirak_PECS = []
            JirakExtrem_MCC = []
            JirakExtrem_PECS = []
            for i in np.arange(nbPeriod):
                Jirak_MCC.append(100*Jirak[i][1])
                Jirak_PECS.append(100*Jirak[i][2])
                JirakExtrem_MCC.append(100*JirakExtrem[i][1])
                JirakExtrem_PECS.append(100*JirakExtrem[i][2])
          
            bar2 = ax.bar(x-0.3, Jirak_MCC, label='MCC', width=0.15, color='red2')
            bar3 = ax.bar(x+0.15, Jirak_PECS, label='PECS', width=0.15, color='yellow2')
            legendObjgeneral = [bar2, bar3]
            
            bar5 = ax.bar(x-0.15, JirakExtrem_MCC, label='MCC', width=0.15, color='red7')
            bar6 = ax.bar(x+0.3, JirakExtrem_PECS, label='PECS', width=0.15, color='yellow7')
            xForLineMCC = []
            xForLinePECS = []
            for i in np.arange(nbPeriod):
                xForLineMCC.append(i-0.15)
                xForLinePECS.append(i+0.3)
            ax.line(xForLineMCC, JirakExtrem_MCC, marker='o', color = 'black')
            ax.line(xForLinePECS, JirakExtrem_PECS, marker='o', color = 'black')
            legendObjproportion = [bar5, bar6]
            
            ax.format(xlim=[-0.6,nbPeriod-0.5],ylim=[0,35],xformatter=bins, xlocator=1,xticklen=0, yformatter='percent', title='Evolution classifation Jirak en proportion : MCC et PECS')
            
            ax.legend(legendObjproportion, ncol=1,loc='ur',title="Population des extrêmes\n(Surf. 235°K > 70000km²)")
            ax.legend(legendObjgeneral, ncol=1,loc='ul',title='Population globale')
        else:
            obj = ax.bar(barLabel, general, label='Population globale (base 100 = 1989-->1997)', align = 'edge', width=-0.25, color=colors[k%5][0])
            xForLineNormal = []
            xForLineExtrem = []
            for i in np.arange(nbPeriod):
                xForLineNormal.append(i-0.125)
                xForLineExtrem.append(i+0.125)
            ax.line(xForLineNormal, general, marker='o', color = 'black')
            ax.bar(barLabel, extrem, label='Population des extrêmes (base 100 = 1989-->1997)', align = 'edge', width=0.25, color=colors[k][1])
            ax.line(xForLineExtrem, extrem, marker='o', color = 'black')
            ylim=[np.minimum(80, np.minimum(np.min(extrem), np.min(general))),np.maximum(150,np.maximum(np.max(extrem), np.max(general)))]
            ax.format(xlocator=1, xminorlocator=0.5, ytickminor=False, ylim=ylim)
            
            ax.legend(ncols=1, loc='ur')
        k += 1

    fileName = 'EvolutionDecennale_MCS_' + str(int(nbVariablesToPlot)) + 'variables.jpg'
    fig.savefig(fileName)