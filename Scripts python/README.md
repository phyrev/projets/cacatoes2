# Répertoire Scripts Python

Contient plusieurs scripts écrits pendant le stage

## PluvioSahelUtils.py

Ensemble de méthodes utilitaires pour la manipulation des données de pluie

Les métodes getDateOverThresholdRain et getDateOverThresholdRainSimple permettent de récupérer une liste de jours où il a plu au delà d'un seuil passé en paramètre

La méthode BuildAnnualIndex permet de construire une indice de pluie sur les extrêmes en ajustant une loi de Gambel simple. Testée avec les données Badoplu uniquement


## CacatoesUtils.py

Ensemble de méthodes utilitaires pour la manipulation des données satellite Cacatoes
Principale méthode appelée pour établir une climatologie des MCS : 

### ComputePlotData_Sahel_Cacatoes(

- **DailyRainSahel**, 				# dataFrame contenant les cumuls de pluie journaliers, la date, la station de mesure 
								
- **villesDictionary**, 				#liste des "stations" pour utilisation avec Badoplu, sinon liste contenant "moyenne" "max" et "std" pour utilisation sur maille AMMA-Catch
								
- **cells**, 							#cellules 1°x1° correspondant à chaque station dans le cas Badoplu, liste contenant 3x la maille 1°x1° du niger pour utilisation AMMA-Catch
								
- **cacatoesVarDict**, 				#liste des variables fournies par Cacatoès que l'on souhaite récupérer
								
- **wantedIndicesDict**, 				#liste des "indices" utilisés pour établir la climatologie des MCS (peut être une variable Cacatoes brute ou un indice construit à partir de celles-ci
								
- **seuilPluvio**, 					#Seuil de pluie à partir duquel on souhaite travailler. Si valeur 0 => on prend tous les jours
								
- **pluvioMax**, 						#permet de filtrer les jours pour lesquels la pluie est supérieure à ce seuil (pas vraiment utilisé)
								
- **listAnneesDict**, 				#liste des années sur lesquelles on souhaite travailler
								
- **progress**, 						#Barre de progression. Utilisée dans ViewSahelStations.ipynb uniquement (fixée à None sinon)
								
- **needCheckVeille** = True, 		# Flag permettant d'aller cherche les MCS de la veille, utile pour travail avec les données Badoplu car les cumul de pluie vont de 6h à 6h et sont à cheval sur 2 jours
								
- **nbMaxMcs** = 25, 					#Permet de fixer un nombre maxi de MCS répertorié dans Cacatoes par jour. Les jours où le nombre est supérieur au seuil sont ignorés (valeur = 25 => pas d'effet car nombre maxi dans cacatoes)
								
- **nbMCSToTakeInIndice** = 25, 		#Nombre de MCS à utiliser pour calculer les indices souhaités. Les résultats présentés dans le rapport utilisent la valeur 1 (valeur = 25 => on utilise tout)
								
- **minStd** = 0.0, 					#Si utilisation avec AMMA-Catch, permet de filtrer les jours selon l'écart type des valeurs de pluie sur la maille 1°x1° (pas utilisé, 0 => aucun filtre)
								
- **maxStd** = 1000.0, 				#Si utilisation avec AMMA-Catch, permet de filtrer les jours selon l'écart type des valeurs de pluie sur la maille 1°x1° (pas utilisé, 1000 => aucun filtre)
								
- **givePrevAndNextDay** = False, 	#Permet de repérer des -épisodes- de pluie dont la durée est supérieure à 1 jours. Pas utilisé dans les résultats du rapport
								
- **seuilWithMoyenne**=True, 			#Dans le cas d'une utilisation avec les données AMMA-Catch, Indique si le seuilPluvio est à considérer sur la moyenne des stations ou sinon sur le max des stations
								
- **pathFilesCacatoes** = '../CACATOES/') #répertoire où sont stockées les données Cacatoes. Aujourd'hui sur un disque dur : D:\archives\MCS_Tracking\data\Stage_FLC\Cacatoes\lon_10W_10E_lat_0N_20N

## Run_MCS_Climatology.py

Script permetant de retrouver les résultats décrits dans le §4.1 du rapport sur la climatologie des MCS, utilisé dans le notebook AMMA-Catch_ClimatoMCS_Only.ipynb

## Run_Badoplu_Tendance.py 

Script permetant de retrouver les résultats décrits dans le §4.2.1 du rapport sur les tendances sur les pluies, utilisé dans le notebook TendancePluieBadoplu_Only.ipynb

## Run_MCS_Tendance.py

Script permetant de retrouver les résultats décrits dans le §4.2.2 du rapport sur les tendances sur les MCS, utilisé dans le notebook TendanceMCS_only.ipynb



