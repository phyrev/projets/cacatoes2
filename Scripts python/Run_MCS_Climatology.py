import pandas as pd
from ipyleaflet import Map, GeoJSON, Marker, CircleMarker, DrawControl, ScaleControl, Rectangle, DivIcon
import ipywidgets as ipyw
from pandas.io.json import json_normalize
import CacatoesUtils
import PluvioSahelUtils
from ipywidgets import HTML
import proplot as pplot
import numpy as np
import datetime
import scipy.stats
import glob
import os

def buildValuesForBarJirak(serie, percent=False):
    factor = 1.0
    if percent==True:
        factor = 100.0
    val0 = factor * len(serie.where(serie == 0).dropna())/len(serie)
    val1 = factor * len(serie.where(serie == 1).dropna())/len(serie)
    val2 = factor * len(serie.where(serie == 2).dropna())/len(serie)
    val3 = factor * len(serie.where(serie == 3).dropna())/len(serie)
    val4 = factor * len(serie.where(serie == 4).dropna())/len(serie)
    val = [val0, val1, val2, val3, val4]
    return val


def GetAllMCSDataFromCacatoesAndAmmaCatch(startYear, endYear, cell, seuilPluvio, wantedIndice, pathFilesBadoplu = '', pathFilesCacatoes = '../CACATOES/AMMA-CATCH/'):    
    
    dailyRainFile = os.path.join(pathFilesBadoplu,'daily_rain.csv')
    metaDataFile = os.path.join(pathFilesBadoplu,'metadata.csv')
    DailyRainSahel = pd.read_csv(dailyRainFile,parse_dates=True, index_col=0)
    RainMetadata = pd.read_csv(metaDataFile,index_col=0)
    listStation = RainMetadata.index
    
    dailyRainFileAMMA = os.path.join(pathFilesBadoplu,'AMMA-Catch_rains_5mn.csv')
    metaDataFileAMMA = os.path.join(pathFilesBadoplu,'AMMA-Catch_metadata.csv')
    AMMA_Catch_Rain = pd.read_csv(dailyRainFileAMMA,parse_dates=True, index_col=0)
    AMMA_Catch_Metadata = pd.read_csv(metaDataFileAMMA,index_col=0)
    listStation = AMMA_Catch_Metadata.index
    
    lonmin = cell[0][0]
    lonmax = cell[0][1]
    latmin = cell[1][0]
    latmax = cell[1][1]
    AMMA_Catch_Rain_OnlyCellStation = AMMA_Catch_Rain
    for s in range(0,listStation.size):
        lon = AMMA_Catch_Metadata['lon'][s]
        lat = AMMA_Catch_Metadata['lat'][s]
        if lon < lonmin or lon > lonmax or lat < latmin or lat > latmax:
            ville = listStation[s]
            AMMA_Catch_Rain_OnlyCellStation = AMMA_Catch_Rain_OnlyCellStation.drop(ville, axis=1)
    
    nbYear = endYear - startYear + 1
    anneesDict = {}
    for y in np.arange(nbYear):
        year = y+startYear
        anneesDict[y] = year
        
    AMMA_CatchDaily_WithMean = np.empty(0)
    for annee in anneesDict.values():
        
        AMMA_Catch_annee = AMMA_Catch_Rain_OnlyCellStation.loc[str(annee)]
        AMMA_CatchDaily_annee = AMMA_Catch_annee.groupby(AMMA_Catch_annee.index.dayofyear).sum()
        
        for badopluStation in RainMetadata.index:
            badoplu_annee = PluvioSahelUtils.getDateOverThresholdRain(DailyRainSahel, badopluStation, annee, 0,1000)
            if len(AMMA_CatchDaily_annee.index) == len(badoplu_annee.index):
                AMMA_CatchDaily_annee.index = badoplu_annee.index
                break

        AMMA_CatchDaily_annee_meanAllStations = AMMA_CatchDaily_annee.mean(axis=1)
        AMMA_CatchDaily_annee_maxAllStations = AMMA_CatchDaily_annee.max(axis=1)
        AMMA_CatchDaily_annee_sdAllStations = np.std(AMMA_CatchDaily_annee, axis=1)
        
        AMMA_CatchDaily_annee_WithMean = pd.concat([AMMA_CatchDaily_annee,AMMA_CatchDaily_annee_meanAllStations,AMMA_CatchDaily_annee_maxAllStations, AMMA_CatchDaily_annee_sdAllStations],axis=1)
        AMMA_CatchDaily_annee_WithMean = AMMA_CatchDaily_annee_WithMean.rename(columns={0 : 'moyenne'})
        AMMA_CatchDaily_annee_WithMean = AMMA_CatchDaily_annee_WithMean.rename(columns={1 : 'max'})
        AMMA_CatchDaily_annee_WithMean = AMMA_CatchDaily_annee_WithMean.rename(columns={2 : 'std'})
        length = len(AMMA_CatchDaily_WithMean)
        if length == 0:
            AMMA_CatchDaily_WithMean = AMMA_CatchDaily_annee_WithMean
        else:
            AMMA_CatchDaily_WithMean = pd.concat([AMMA_CatchDaily_WithMean,AMMA_CatchDaily_annee_WithMean],axis=0)
            
    villesDictionary = {}
    villesDictionary[0] = 'moyenne'
    villesDictionary[1] = 'max'
    villesDictionary[2] = 'std'
    cells = {}
    cells['moyenne'] = cell
    cells['max'] = cell
    cells['std'] = cell
    variablesDictionary = {}
    variablesDictionary[0] = 'DAILYmcs_Pop'
    variablesDictionary[1] = 'QCmcs_Label'
    variablesDictionary[2] = 'INT_Distance'
    variablesDictionary[3] = 'INT_Smax_235K'
    variablesDictionary[4] = 'INT_GridFraction_235K'
    variablesDictionary[5] = 'INT_Smax_210K'
    variablesDictionary[6] = 'INT_GridFraction_200K'
    variablesDictionary[7] = 'INT_gridtimeOccupation_start'
    variablesDictionary[8] = 'INT_gridtimeOccupation_end'
    variablesDictionary[9] = 'INT_Duration'
    variablesDictionary[10] = 'INIT_Time'
    variablesDictionary[11] = 'END_Time'
    variablesDictionary[12] = 'INT_Tmax'
    variablesDictionary[13] = 'INT_Sfract_210K'
    variablesDictionary[14] = 'INT_Tbmin'
    variablesDictionary[15] = 'INT_GridFraction_210K'
    variablesDictionary[16] = 'INT_CLASSIF_JIRAK'
    variablesDictionary[17] = 'INIT_Lon'
    variablesDictionary[18] = 'INIT_Lat'
    variablesDictionary[19] = 'END_Lon'
    variablesDictionary[20] = 'END_lat'
    variablesDictionary[21] = 'QCmcs_Class'
    variablesDictionary[22] = 'INT_Scum'
    variablesDictionary[23] = 'INT_Surf210K_at_Tmax'
    variablesDictionary[24] = 'INT_Tbavg208K_at_Tmax'
    variablesDictionary[25] = 'INT_Ecc220K_at_Tmax'
    variablesDictionary[26] = 'INT_orientation220K_at_Tmax'
    variablesDictionary[27] = 'QCmcs_Flag'
    variablesDictionary[28] = 'INT_Lon_at_Tmax'
    variablesDictionary[29] = 'INT_Lat_at_Tmax'
    needCheckVeille = False
    seuilPluvioMax = seuilPluvio
    maxPluvioAbsolu = 1000
    maxNBMCS_totake = 25
    maxNBMCS_forIndice = 1
    minStd = 0.0
    maxStd = 1000
    givePrevAndNextDay = False
    seuilWithMoyenne = True
    wantedCacatoesIndice = {}
    for key in wantedIndice:
        wantedCacatoesIndice[key] = wantedIndice[key][0]
    t1, t2, t3, t4 = CacatoesUtils.ComputePlotData_Sahel_Cacatoes(AMMA_CatchDaily_WithMean, villesDictionary, cells, variablesDictionary, 
                                                              wantedCacatoesIndice, seuilPluvioMax, maxPluvioAbsolu, anneesDict, None, needCheckVeille, 
                                                              maxNBMCS_totake,maxNBMCS_forIndice,minStd, maxStd, givePrevAndNextDay, seuilWithMoyenne,
                                                              pathFilesCacatoes)
    return t1, t2, t3, t4
    

    
def PlotMCSClimatology(t1, t2, wantedIndice, percentilesToPlot):
    serieMoyenne = t1['pluie moyenne']
    serieMax = t1['pluie max']
    nbPercentileToPLot = len(percentilesToPlot)
    

    buildGraphWithMoyenne = True
    strType = ' en pluie moyenne = '
    if buildGraphWithMoyenne==True:
        serieToUse = serieMoyenne
    else:
        serieToUse = serieMax
        strType = ' en pluie max = '
    valuePercentile = np.zeros(nbPercentileToPLot)
    indiceExtrems = []
    for p in np.arange(nbPercentileToPLot):
        valuePercentile[p] = np.percentile(serieToUse,percentilesToPlot[p])
        print('Valeur pour le percentile :',percentilesToPlot[p],strType,valuePercentile[p],'mm')
        indiceExtrems.append(np.where(serieToUse > valuePercentile[p])[0])
    
    indicesBassesValeurs = np.where(serieToUse <= valuePercentile[0])[0]
    
    labelExtrem = 'Valeur > percentile ' + str(percentilesToPlot[0])
    labelBasseValeur = 'Valeur <= percentile ' + str(percentilesToPlot[0])
    for key in t2:
        if key == 'NB MCS':
            continue
        for ikey in wantedIndice:
            if wantedIndice[ikey][0] == key:
                wantedNameForIndice = wantedIndice[ikey][0]
                wantedXLabel = wantedIndice[ikey][2]
                legendPos = wantedIndice[ikey][3]
                break
        
        nbCols = 2
        if key == 'Jirak':
            nbCols = 1
        pplot.rc.abc = 'a.'
        pplot.rc.titleloc = 'l'
        pplot.rc['legend.fontsize'] = 'small'
        gs = pplot.GridSpec(nrows=1, ncols=nbCols)
        fig = pplot.figure(axwidth=4, axheight=2, share=False, suptitle = wantedNameForIndice)
        
        CompleteSerie = pd.Series(t2[key], name=key)
        ExtremSerie = []
        for p in np.arange(nbPercentileToPLot):
            ExtremSerie.append(pd.Series(t2[key][indiceExtrems[p]], name=wantedXLabel))
        
        medianExtremSerie_0 = np.nanmedian(ExtremSerie[0])
        
        BassesValeursSerie = pd.Series(t2[key][indicesBassesValeurs], name=key)
        medianBassesValeursSerie = np.nanmedian(BassesValeursSerie)
        
        minValueHisto = np.min(t2[key])
        maxValueHisto = 1.1 * np.max(t2[key])
        freqValueForRepartition = (maxValueHisto -  minValueHisto)/ 1000
        freqValueForHisto = (maxValueHisto -  minValueHisto)/ 30
        if key == 'Durée sur cellule' or key == 'INT_Tbavg208K_at_Tmax':
            freqValueForHisto = 1
        binsForRepartition = pplot.arange(minValueHisto, maxValueHisto, freqValueForRepartition)
        binsForHisto = pplot.arange(minValueHisto, maxValueHisto, freqValueForHisto)
        
        stat, pValue, med, tbl = scipy.stats.median_test(ExtremSerie[0], BassesValeursSerie) 
        """statKS_1, pValueKS_1 = scipy.stats.ks_2samp(ExtremSerie_e1, BassesValeursSerie, alternative='less')
        statKS_2, pValueKS_2 = scipy.stats.ks_2samp(ExtremSerie_e2, BassesValeursSerie, alternative='less')
        statKS_3, pValueKS_3 = scipy.stats.ks_2samp(ExtremSerie_e3, BassesValeursSerie, alternative='less')
        statKS_4, pValueKS_4 = scipy.stats.ks_2samp(ExtremSerie_e3, ExtremSerie_e1, alternative='less')
        print(key)
        print('pValue 90 vs BV',pValueKS_1)
        print('pValue 95 vs BV',pValueKS_2)
        print('pValue 99 vs BV',pValueKS_3)
        print('pValue 99 vs 90',pValueKS_4)"""
        labelComparaison = 'Distributions "extrême" (p' +  str(percentilesToPlot[0]) + ') et "non extrême"' + ' : pValue = ' + np.format_float_scientific(pValue, precision=2)
        
        if key == 'Jirak':
            labelComparaison = 'Distributions "extrême" (> p90, > p95 et > p99) et "non extrême" (< p90)'
        ax = fig.subplot(gs[0,0],title = labelComparaison, ylabel='Densité de probabilité')
        
        if key != 'Jirak':
            title2 = "Evolution de la fonction de répartition selon les extrêmes"
            ax2 = fig.subplot(gs[0,1], title = title2, ylabel='Probabilité')
            histo = ax2.hist( t2[key], binsForRepartition, density=True, cumulative=True, histtype='step', color='blue9', lw=1.5, label = "Jours de pluie : moy. > 0,5mm")

        xMin = minValueHisto
        xMax = minValueHisto + np.argmax(histo[0]) * freqValueForRepartition
        
        if key=='Temp brightness mini':
            xMin = 170
        if key == 'INT_Tbavg208K_at_Tmax':
            xMin = 190
            xMax = 219
        if key == 'Fraction cell index 235K':
            xMax = 60
        if key == 'Durée sur cellule':
            xMax = 18
        if key == 'Grid fraction 235K x Smax 235':
            xMax = 300000
        if key == 'Smax 235 index':
            xMax = 700000
        
        if key=='Jirak':
            bins = ['','None Jirak', 'MCC', 'PECS', 'MβMCC', 'MβPECS']
            x = np.arange(5)
            valForBarBassesValeurs = buildValuesForBarJirak(BassesValeursSerie)
            valForBarExtrem = []
            for p in np.arange(nbPercentileToPLot):
                valForBarExtrem.append(buildValuesForBarJirak(ExtremSerie[p]))
            
            
            bar1 = ax.bar(x-0.3, valForBarBassesValeurs, color='indigo9',alpha=0.6, width=0.2, label=labelBasseValeur)
            for p in np.arange(nbPercentileToPLot):
                offset = -0.1 + p * 0.2
                color = 'red' + str(int(3 + 3 * p))
                if p > 2:
                    color = black
                label="Extrême p" + str(percentilesToPlot[p]) + ' : moy. > ' + str(int(np.around(valuePercentile[p],0)))  + 'mm'
                bar2 = ax.bar(x+offset, valForBarExtrem[p], color=color, alpha=0.6, width=0.2, label=label)
                
            ax.format(xlim=[-1,5],xformatter=bins, xticklen=0, ylabel='Proportion')
        else:
            histo1 = ax.hist( ExtremSerie[0], binsForHisto, filled=True, label=labelExtrem, density=True, alpha=0.5, color='red9')
            median_lab = 'mediane=' + str(np.around(medianExtremSerie_0,2))

            ax.vlines([medianExtremSerie_0,medianExtremSerie_0],[0,histo1[0].max()],lw=2, label = median_lab, color='red9')
            histo2 = ax.hist( BassesValeursSerie, binsForHisto, filled=True, label=labelBasseValeur, density=True, alpha=0.5, color='indigo9')
            median_lab = 'mediane=' + str(np.around(medianBassesValeursSerie,2))
            ax.vlines([medianBassesValeursSerie,medianBassesValeursSerie],[0,histo2[0].max()],lw=2, label = median_lab, color='indigo9')
            ax.format(xlim=[xMin, xMax])
            if key == 'Smax 235 index' or key == 'Grid fraction 235K x Smax 235':
                ax.format(xformatter='sci', yformatter='sci')
        ax.legend(ncols=1, loc=legendPos[0])
        if key != 'Jirak':
            label='Basses valeurs : moy. < ' + str(int(np.around(valuePercentile[0],0)))  + 'mm'
            histo3 = ax2.hist( BassesValeursSerie, binsForRepartition, density=True, cumulative=True, histtype='step', color='blue5', lw=1.5, label=label)
            for p in np.arange(nbPercentileToPLot):
                color = 'red' + str(int(3 + 3 * p))
                if p > 2:
                    color = black
                label="Extrême p" + str(percentilesToPlot[p]) + ' : moy. > ' + str(int(np.around(valuePercentile[p],0)))  + 'mm'
                histo4 = ax2.hist( ExtremSerie[p], binsForRepartition, density=True, cumulative=True, histtype='step', color=color, lw=1.5, label=label)
            
            
            if key=='Temp brightness mini':
                xMin = 170
            if key == 'INT_Tbavg208K_at_Tmax':
                xMin = 195
                xMax = 215
            if key == 'Fraction cell index 235K':
                xMax = 60
            ax2.format(xlim=[xMin, xMax], xlabel=wantedXLabel)
            if key == 'Smax 235 index' or key == 'Grid fraction 235K x Smax 235':
                ax2.format(xformatter='sci')
            ax2.legend(loc = legendPos[1], ncol=1)
        fileName = 'EvolutionDistribution_' + key + '.jpg'
        fig.savefig(fileName)
