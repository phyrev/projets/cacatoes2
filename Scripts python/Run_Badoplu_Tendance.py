import pandas as pd
from ipyleaflet import Map, GeoJSON, Marker, CircleMarker, DrawControl, ScaleControl, Rectangle, DivIcon
import ipywidgets as ipyw
from pandas.io.json import json_normalize
import CacatoesUtils
import PluvioSahelUtils
from ipywidgets import HTML
import proplot as pplot
import numpy as np
import datetime
import scipy.stats
import glob
import os

def GetAllRainDataFromBadoplu(startYear, endYear, GeographicArea, pathFilesBadoplu = ''):
    dailyRainFile = os.path.join(pathFilesBadoplu,'daily_rain.csv')
    metaDataFile = os.path.join(pathFilesBadoplu,'metadata.csv')
    
    DailyRainSahel = pd.read_csv(dailyRainFile,parse_dates=True, index_col=0)
    RainMetadata = pd.read_csv(metaDataFile,index_col=0)
    listStation = RainMetadata.index

    nbStations = len(listStation)
    villesDictionary = {}
    cells = {}
    v = 0
    marge = 0.0
    lonMin = GeographicArea[0][0]
    lonMax = GeographicArea[0][1]
    latMin = GeographicArea[1][0]
    latMax = GeographicArea[1][1]
    for i in np.arange(nbStations):
        ville = listStation[i]
        cell = CacatoesUtils.getCellForStation(RainMetadata, ville)
        lon_ville = RainMetadata['lon'][i]
        lat_ville = RainMetadata['lat'][i]
        if lat_ville < latMin or lat_ville > latMax:
            continue
        if lon_ville < lonMin or lon_ville > lonMax:
            continue
        if lon_ville > cell[0][0] + marge and lon_ville < cell[0][1] - marge and lat_ville > cell[1][0] + marge and lat_ville < cell[1][1] - marge:
            villesDictionary[v] = ville
            cells[ville] = cell
            print('maille pour ', ville,'(',lon_ville,',',lat_ville,'): ',cells[ville])
            v+=1
    anneesDict = {}
    nbYear = endYear - startYear + 1
    for i in np.arange(nbYear):
        anneesDict[i] = startYear + i
    
    variablesDictionary = {}
    wantedIndice = {}
    needCheckVeille = True
    seuilPluvioMax = 10
    maxPluvioAbsolu = 1000
    maxNBMCS_totake = 25
    maxNBMCS_forIndice = 1
    minStd = 0.0
    maxStd = 1000
    givePrevAndNextDay = False
    seuilWithMoyenne = True
    t1, t2, t3, t4 = CacatoesUtils.ComputePlotData_Sahel_Cacatoes(DailyRainSahel, villesDictionary, cells, variablesDictionary, wantedIndice, 
                                                                    seuilPluvioMax, maxPluvioAbsolu, anneesDict, None, needCheckVeille, maxNBMCS_totake,maxNBMCS_forIndice,
                                                                    minStd, maxStd, givePrevAndNextDay, seuilWithMoyenne,'../CACATOES/AMMA-CATCH/')
    
    dfPluie = t1.copy()
    pd.set_option('mode.chained_assignment',None)
    dfPluie = AjustRainDataWithMeanByVille(dfPluie)
    dfPluie.index = dfPluie['date']
    dfPluie = dfPluie.drop(['date'], axis=1)
    
    return dfPluie

def AjustRainDataWithMeanByVille(df):
    pd.set_option('mode.chained_assignment',None)
    triStation = df.groupby(df['Ville']).mean()
    listMoyenneStation = triStation['pluie moyenne']
    for i in df.index:
        ville = df['Ville'][i]
        moyenneVille = listMoyenneStation[ville]
        
        newVal = df['pluie max'][i] / moyenneVille
        df['pluie max'][i] = newVal
    return df
    
def BuildRainPerYearTab(df):
    dfRainYear = {}
    sizeDf = len(df.index)
    minYear = df.index[0].year
    maxYear = df.index[sizeDf - 1].year
    nbYear = maxYear - minYear + 1
    nbMCSYear = np.zeros(nbYear)
    xYearInGraph = np.arange(minYear,maxYear+1,1)
    for y in np.arange(nbYear):
        year = y+minYear
        before = year-1
        after = year+1
        indexYear = np.intersect1d(np.where(df.index.year > before),np.where(df.index.year < after))
        dfRainYear[year] = df.iloc[indexYear]
    return xYearInGraph, dfRainYear
    
def BuildSubDataFrameForPeriod(dfGlobal, nbPeriod):
    sizeDf = len(dfGlobal.index)
    minYear = dfGlobal.index[0].year
    maxYear = dfGlobal.index[sizeDf - 1].year
    nbYear = maxYear - minYear + 1
    lenPeriod = (int) (nbYear / nbPeriod)
    startPeriods = np.zeros(nbPeriod)
    endPeriods = np.zeros(nbPeriod)
    for i in np.arange(nbPeriod):
        startPeriods[i] = minYear + i * lenPeriod
        endPeriods[i] = minYear + (i+1) * lenPeriod - 1
        if i == 0:
            subDf = dfGlobal.iloc[np.where(dfGlobal.index.year < startPeriods[i])[0]]
    subDfDict = {}
    subDfDict[0] = dfGlobal.iloc[np.where(dfGlobal.index.year < startPeriods[1])[0]]
    for i in np.arange(1,nbPeriod-1,1):
        inter = np.intersect1d(np.where(dfGlobal.index.year > endPeriods[i-1]),np.where(dfGlobal.index.year < startPeriods[i+1]))       
        subDfDict[i] = dfGlobal.iloc[inter]
    subDfDict[nbPeriod-1] = dfGlobal.iloc[np.where(dfGlobal.index.year > endPeriods[nbPeriod-2])[0]]
    return subDfDict, startPeriods, endPeriods
    
def PlotRainTendance(dfPluie, nbPeriod, ratioToUse):
    xYear, dfYear = BuildRainPerYearTab(dfPluie)
    subDf, startPeriods, endPeriods = BuildSubDataFrameForPeriod(dfPluie, nbPeriod)
    barLabel = []
    indiceValues = []
    xForLine = []
    nbYearInPeriod = endPeriods[0] - startPeriods[0] + 1
    nbYearTotal = int(nbYearInPeriod * nbPeriod)
    nb0 = len(np.where(subDf[0]['pluie max'] > ratioToUse)[0])
    for i in np.arange(nbPeriod):
        barLabel.append(str(int(startPeriods[i])) + ' --> ' + str(int(endPeriods[i])))
        xForLine.append(i)
        
        if i == 0:
            indiceValues.append(100)
        else:
            nb_i = len(np.where(subDf[i]['pluie max'] > ratioToUse)[0])
            indiceValues.append(100 * nb_i/nb0)
    listNbYear = []
    for annee in dfYear:
        nbYear = len(np.where(dfYear[annee]['pluie max'] > ratioToUse)[0])
        listNbYear.append(nbYear)
        
        
    Ylim = [80, 1.1 * np.max(indiceValues)]
    pplot.rc.abc = 'a.'
    pplot.rc.titleloc = 'l'
    pplot.rc['legend.fontsize'] = 'medium'
    gs = pplot.GridSpec(nrows=1, ncols = 2, wratios=(2, 1))
    Suptitle = 'Nombre de jours de pluie supérieur à ' + str(ratioToUse) + 'x la moyenne : Evolution décennale'
    fig = pplot.figure(refaspect=2, refwidth=5, share=False, suptitle=Suptitle)
    ax2 = fig.subplot(gs[0,0],title='Valeurs annuelles',ylabel='Nb de jours',xlabel='Années')
    titleAx = 'Par période de ' + str(int(nbYearInPeriod)) + ' ans'
    ax = fig.subplot(gs[0,1], title=titleAx)
    
    labelForBars = "Indice nombre de jours\n(100 <=> période " + barLabel[0]
    obj = ax.bar(barLabel, indiceValues, label=labelForBars, width=0.5, color='grey5')
    ax.line(xForLine, indiceValues, marker='o', color = 'black')
    ax.format(xlocator=1, xminorlocator=0.5, xticklen=0, ytickminor=False,ylim=Ylim)
    ax.legend(ncols=1, loc='ur')

    yLimNbMCS=[np.min(listNbYear),np.max(listNbYear)]
    ax2.line(xYear, listNbYear, marker='o', color = 'black', label='Nombre de jours par an')
    mobile = np.convolve(listNbYear, [1, 1, 1,1,1], 'valid') / 5
    maxPlotXForMobile = nbYearTotal-2
    ax2.line(xYear[2:maxPlotXForMobile], mobile, color = 'red', label='Moyenne mobile sur 5 ans')
    ax2.format(ytickminor=False,ylim=yLimNbMCS)
    ax2.legend(ncols=1, loc='lr')
    fileName = 'EvolutionDecennale_Rain_NbTotal.jpg'
    fig.savefig(fileName)