import numpy as np 
import pandas as pd
import datetime

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

def moving_average_withsamedimension(x, w):
    if w%2 == 0:
            raise Exception("la portée de la moyenne mobile doit être impaire !")
    mov_av = moving_average(x, w)
    moyenne_mobile_full = np.empty(mov_av.size + w - 1)
    offset = (w-1)//2
    for i in range(0,offset):
        moyenne_mobile_full[i] = np.NAN
        moyenne_mobile_full[moyenne_mobile_full.size - 1 - i] = np.NAN
    for j in range (offset,moyenne_mobile_full.size - offset):
        moyenne_mobile_full[j] = mov_av[j-offset]
    return moyenne_mobile_full

def getDataFromStation(dataBase, stationName):
    listStation = dataBase['metadata'].index
    for s in range(0,listStation.size):
        if (listStation[s] == stationName):
            data_station = dataBase['daily_rains'][listStation[s]]
            break
    return data_station

def computeExtremeData(data, minYear, maxYear):
    list_annees = np.empty(maxYear - minYear + 1)
    max_value = np.empty(maxYear - minYear + 1)
    mean_significant_rain = np.empty(maxYear - minYear + 1)
    nb_days_over_treshold = np.empty(maxYear - minYear + 1)
    for i in range(minYear,maxYear+1):
        k = i - minYear
        data_annee = data.iloc[lambda x: x.index.year == i]
        maxi = np.amax(data_annee)
        max_value[k] = maxi
        list_annees[k] = i
        somme_significant_rain = 0
        nb_significant_rain = 0
        nb_days_over_treshold[k] = computeNbDaysOverTreshold(data_annee, 2)
    return list_annees, max_value, nb_days_over_treshold

def computeNbDaysOverTreshold(oneYearData, ratioTreshold):
    somme_significant_rain = 0
    nb_significant_rain = 0
    nb_days_over_treshold = 0
    for j in range(0,oneYearData.size):
        rain_day = oneYearData[j]
        if rain_day > 1 :
            somme_significant_rain = somme_significant_rain + rain_day
            nb_significant_rain = nb_significant_rain + 1
    mean_significant_rain = 1000
    if nb_significant_rain > 0:
        mean_significant_rain = somme_significant_rain / nb_significant_rain
    for j in range(0,oneYearData.size):
        rain_day = oneYearData[j]
        if rain_day > ratioTreshold * mean_significant_rain:
            nb_days_over_treshold = nb_days_over_treshold + 1
    
    return nb_days_over_treshold

def empiricRepartitionFunction(maximumDatas):
    maximumDatasNotNan = maximumDatas[np.logical_not(np.isnan(maximumDatas))]
    maximumSortedDatas = np.sort(maximumDatasNotNan)
    N = maximumSortedDatas.size
    empiricRepartition = np.empty(N)
    for i in range(0,N):
        empiricRepartition[i] = (i+0.5)/(N+0.5)
    df = pd.DataFrame(empiricRepartition, index=maximumSortedDatas,columns=['empiricRep'])
    return df

def GumbelEstimator(empiricRepartition):
    repartitionDatas = empiricRepartition['empiricRep']
    repDatasLn = np.log(-np.log(repartitionDatas))
    reg,cov = np.polyfit(repDatasLn.index,repDatasLn.values,1,cov=True)
    a = -1/reg[0]
    b = reg[1]*a
    return a,b

def GumbelLawValues(extremValues, aGumbel, bGumbel):
    normalizedValues = (extremValues - bGumbel) / aGumbel
    gumbelValues = np.exp(-np.exp(-normalizedValues))
    return gumbelValues

def BuildAnnualIndex(dataRain, metaData, minYear, maxYear, zoneLim = [[-5,10],[0,15]]):
    annual_index = np.zeros(maxYear - minYear + 1)
    nb_significant_values = np.zeros(maxYear - minYear + 1)
    selectedMetadata, selectedDataRain = StationFilter(dataRain, metaData, zoneLim)
    listStation = selectedMetadata.index
    print('construction indice sur ', listStation.size, ' stations')
    print(listStation)
    for s in range(0,listStation.size):
        data_station = selectedDataRain[listStation[s]]
        list_annees, max_value, nb_days_over_treshold = computeExtremeData(data_station, minYear, maxYear)
        df = empiricRepartitionFunction(max_value)
        a,b = GumbelEstimator(df)
        gumbelValues = GumbelLawValues(df.index, a, b)

        for i in range (0, 2016 - 1960 + 1):
            if np.isnan(max_value[i])==False:
                annual_index[i] = annual_index[i] + GumbelLawValues(max_value[i],a,b)
                nb_significant_values[i] = nb_significant_values[i] + 1

    annual_index = annual_index / nb_significant_values
    return annual_index,listStation, list_annees

def StationFilter(dataRain, metaData, zoneLim):
    maskLon = (metaData['lon'] <= zoneLim[0][1]) * (metaData['lon'] >= zoneLim[0][0]) 
    maskLat = (metaData['lat'] >= zoneLim[1][0]) * (metaData['lat'] <= zoneLim[1][1])
    mask = maskLon * maskLat
    selectedMetadata = metaData[mask]
    selectedDataRain = dataRain[selectedMetadata.index]
    return selectedMetadata, selectedDataRain
    
def df_to_geojson(df, lat='latitude', lon='longitude'):
    # create a new python dict to contain our geojson data, using geojson format
    geojson = {'type':'FeatureCollection', 'features':[]}

    # loop through each row in the dataframe and convert each row to geojson format
    r = 0
    for _, row in df.iterrows():
        # create a feature template to fill in
        feature = {'type':'Feature',
                   'properties':{},
                   'geometry':{'type':'Point',
                               'coordinates':[]}}

        # fill in the coordinates
        feature['geometry']['coordinates'] = [row[lon],row[lat]]
        feature['properties']['name'] = df.index[r]
        feature['properties']['selected'] = False
        r = r+1
        # add this feature (aka, converted dataframe row) to the list of features inside our dict
        geojson['features'].append(feature)
    
    return geojson
    
def getDateOverThresholdRain(dataBase, stationName, year, threshold, maxValue = 1000000, getMeanValue = False):
    dataStation = dataBase[stationName]
    if getMeanValue == True:
        dataMean = dataBase['moyenne']
    indicesyear = np.where(dataStation.index.year == year)
    dataStationYear = dataStation[indicesyear[0]]
    if getMeanValue == True:
        dataMeanYear = dataMean[indicesyear[0]]
    indiceOverThreshold = np.where(dataStationYear >= threshold)
    indiceUnderMaxValue = np.where(dataStationYear < maxValue)
    
    indiceToTake = np.intersect1d(indiceOverThreshold[0], indiceUnderMaxValue[0])
    if getMeanValue == True:
        resultPluie = dataMeanYear[indiceToTake]
    else:
        resultPluie = dataStationYear[indiceToTake]
    return resultPluie
    
def getDateOverThresholdRainSimple(dataBase, stationName, year, threshold, maxValue = 1000000):
    dataStation = dataBase[stationName]
    
    indicesyear = np.where(dataStation.index.year == year)
    dataStationYear = dataStation[indicesyear[0]]
    nbValueForStationYear = len(dataStationYear)
    indiceOverThreshold = np.where(dataStationYear >= threshold)
    indiceUnderMaxValue = np.where(dataStationYear < maxValue)
    
    indiceToTake = np.intersect1d(indiceOverThreshold[0], indiceUnderMaxValue[0])
    resultPluie = dataStationYear[indiceToTake]
    return resultPluie, nbValueForStationYear
    
def getValuesForListDates(dataBase, dates, stationName, year, getPrevAndNextDay = False):
    dataStation = dataBase[stationName]
    indicesWanted = np.empty(len(dates),dtype=int)
    
    indicesyear = np.where(dataStation.index.year == year)
    dataStationYear = dataStation[indicesyear[0]]
    i = 0
    if getPrevAndNextDay==True:
        indicesWithPrevNext = np.empty(3*len(dates),dtype=int)
    for date in dates:
        x = np.where(dataStationYear.index.month == date.month)
        y = np.where(dataStationYear.index.day == date.day)
        indicesWanted[i] = np.intersect1d(x[0], y[0])       
        if getPrevAndNextDay == True:
            indicesWithPrevNext[3*i + 1] = indicesWanted[i]
            prevDay = date-datetime.timedelta(days=1)
            nextDay = date+datetime.timedelta(days=1)
            xPrev = np.where(dataStationYear.index.month == prevDay.month)
            yPrev = np.where(dataStationYear.index.day == prevDay.day)
            indicesWithPrevNext[3*i] = np.intersect1d(xPrev[0], yPrev[0])
            xNext = np.where(dataStationYear.index.month == nextDay.month)
            yNext = np.where(dataStationYear.index.day == nextDay.day)
            indicesWithPrevNext[3*i + 2] = np.intersect1d(xNext[0], yNext[0])
                       
        i += 1
    if getPrevAndNextDay==True:
        indicesWithPrevNext = np.unique(indicesWithPrevNext)
        resultValues = dataStationYear[indicesWithPrevNext]
    else:
        resultValues = dataStationYear[indicesWanted]
    return resultValues
    
def saveAMMA_Catch_RainValuesForDate(dailyRainDataBase, metadata, date, outputFile):
    #date au format : 'yyyy-mm-dd'
    #outputFile au format : "../QGis/rain_2008-08-05.csv"
    pluie = dailyRainDataBase.loc[date].transpose()
    pluie = pluie.drop(labels=['moyenne','max','std'],axis=0)
    df = pd.DataFrame(metadata, copy=True)
    df = df.drop(labels=['Koyria','Boubon','Torodi','Bololadie','Kaligorou','Gamonzon','Sandidey','Koure_Kobade'],axis=0)
    df = pd.concat([df,pluie],axis=1)
    day = df.columns[2]
    df = df.rename(columns={day : 'pluie'})
    df.index.name = 'Station'
    df.to_csv(outputFile)
    return True