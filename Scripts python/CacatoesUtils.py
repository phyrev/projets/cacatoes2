import numpy as np 
import netCDF4
import glob
import datetime
import calendar
import pandas as pd
import PluvioSahelUtils
import ipywidgets as ipyw

def getCellForStation(metadata, station):
    listStation = metadata.index
    for s in range(0,listStation.size):
        if (listStation[s] == station):
            lon = metadata['lon'][s]
            lat = metadata['lat'][s]
            break
    box = [[np.floor(lon),np.floor(lon)+1],[np.floor(lat),np.floor(lat)+1]]
    return box
    

def open_CACATOES_EUMETSAT(file,lonmin_REG,lonmax_REG,latmin_REG,latmax_REG):

    fh = netCDF4.Dataset(file,'r')
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    
    idlon= np.where( (lon >= lonmin_REG) & (lon <= lonmax_REG) )
    idlat= np.where( (lat >= latmin_REG) & (lat <= latmax_REG) )
    
    lon=lon[idlon]
    lat=lat[idlat]
    
    #print(fh.variables)
    time   		                	= fh.variables['time'][:]
    DAILYmcs_Pop    				= fh.variables['DAILYmcs_Pop'][:,:,:]
    QCmcs_Label  			    	= fh.variables['QCmcs_Label'][:,:,:,:]
    QCtoocan_interruption 			= fh.variables['QCtoocan_Interruption'][:,:,:]
    QCtoocan_nbSegmentedImages      = fh.variables['QCtoocan_nbSegmentedImages'][:,:,:]
    INT_Smax		 		     	= fh.variables['INT_Smax_235K'][:,:,:,:]
    INT_Distance		 		     	= fh.variables['INT_Distance'][:,:,:,:]
    #INT_Smax_235K                   = fh.variables['INT_Duration'][:,:,:,:]
    INT_Tbmin                       = fh.variables['INT_Tbmin'][:,:,:,:]
    INT_Ecc220K_at_Tmax             = fh.variables['INT_Ecc220K_at_Tmax'][:,:,:,:]
    INT_Tbavg208K_at_Tmax           = fh.variables['INT_Tbavg208K_at_Tmax'][:,:,:,:]
    INT_Duration		 			= fh.variables['INT_Duration'][:,:,:,:]
    INT_GridFraction				= fh.variables['INT_GridFraction_235K'][:,:,:,:]
    INT_GridFraction_220K			= fh.variables['INT_GridFraction_220K'][:,:,:,:]
    INT_GridFraction_210K			= fh.variables['INT_GridFraction_210K'][:,:,:,:]
    INT_CLASSIF_JIRAK              	= fh.variables['INT_CLASSIF_JIRAK'][:,:,:,:] 
    INIT_Time = fh.variables['INIT_Time'][:,:,:,:]
    END_Time = fh.variables['END_Time'][:,:,:,:]
    INT_Tmax = fh.variables['INT_Tmax'][:,:,:,:]
    INT_gridtimeOccupation_start = fh.variables['INT_gridtimeOccupation_start'][:,:,:,:]
    INT_gridtimeOccupation_end = fh.variables['INT_gridtimeOccupation_end'][:,:,:,:]
    mat_lonCACATOES 				= np.zeros((len(lat),len(lon)))
    mat_latCACATOES 				= np.zeros((len(lat),len(lon)))
  

    for i in range(0,np.shape(mat_lonCACATOES)[0],1): 
        mat_lonCACATOES[i,:]    	= lon
        
    for j in range(0,np.shape(mat_latCACATOES)[1],1):  
        mat_latCACATOES[:,j]   	= lat
        
    fh.close()
    del(fh)
        
    idlatMax = idlat[0][-1] + 1
    idlonMax = idlon[0][-1] + 1
    
    subDAILYmcs_Pop = DAILYmcs_Pop[:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    
    subQCtoocan_nbSegmentedImages = QCtoocan_nbSegmentedImages[:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subQCtoocan_interruption = QCtoocan_interruption[:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subQCmcs_Label = QCmcs_Label[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    
    subINT_Duration = INT_Duration[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Smax = INT_Smax[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Ecc220K_at_Tmax = INT_Ecc220K_at_Tmax[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Tbavg208K_at_Tmax = INT_Tbavg208K_at_Tmax[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Tbmin = INT_Tbmin[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Distance = INT_Distance[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_GridFraction = INT_GridFraction[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_GridFraction_220K = INT_GridFraction_220K[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_GridFraction_210K = INT_GridFraction_210K[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_CLASSIF_JIRAK = INT_CLASSIF_JIRAK[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINIT_Time = INIT_Time[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subEND_Time = END_Time[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_Tmax = INT_Tmax[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_gridtimeOccupation_start = INT_gridtimeOccupation_start[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    subINT_gridtimeOccupation_end = INT_gridtimeOccupation_end[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    return (time,mat_lonCACATOES,mat_latCACATOES,subDAILYmcs_Pop,
            subINT_Ecc220K_at_Tmax,
            subINT_Tbavg208K_at_Tmax,
            subQCmcs_Label, subINT_Duration,
            subINT_Smax,
            subINT_Distance,
            subINT_Tbmin,
            subINT_GridFraction_220K,
            subINT_GridFraction_210K,
            subINT_CLASSIF_JIRAK,subINIT_Time,subEND_Time,subINT_Tmax,subINT_gridtimeOccupation_start,subINT_gridtimeOccupation_end)

def GET_CACATOES_EUMETSAT_FOR_VARIABLES(file,lonmin_REG,lonmax_REG,latmin_REG,latmax_REG,variablesDictionary):

    fh = netCDF4.Dataset(file,'r')
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    
    idlon= np.where( (lon >= lonmin_REG) & (lon <= lonmax_REG) )
    idlat= np.where( (lat >= latmin_REG) & (lat <= latmax_REG) )
    
    lon=lon[idlon]
    lat=lat[idlat]
    
    #print(fh.variables)
    time   		                	= fh.variables['time'][:]
    nbVariables = len(variablesDictionary)
    DictCacatoesAllValues = {}
    DictCacatoesResultValues = {}
    
    for key in variablesDictionary:
        varName = variablesDictionary[key][0]
        cacatoesValues = fh.variables[varName][:,:,:,:]
        DictCacatoesAllValues[varName] = cacatoesValues
    
    mat_lonCACATOES 				= np.zeros((len(lat),len(lon)))
    mat_latCACATOES 				= np.zeros((len(lat),len(lon)))
  

    for i in range(0,np.shape(mat_lonCACATOES)[0],1): 
        mat_lonCACATOES[i,:]    	= lon
        
    for j in range(0,np.shape(mat_latCACATOES)[1],1):  
        mat_latCACATOES[:,j]   	= lat
        
    fh.close()
    del(fh)
        
    idlatMax = idlat[0][-1] + 1
    idlonMax = idlon[0][-1] + 1
    
    for key in variablesDictionary:
        varName = variablesDictionary[key][0]
        DictCacatoesResultValues[varName] = DictCacatoesAllValues[varName][:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax] 
    
    return DictCacatoesResultValues
            
            
def get_CACATOES_Variable(file,variable, cell):

    fh = netCDF4.Dataset(file,'r')
    
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    print(shape(lon))
    lonmin = cell[0][0]
    lonmax = cell[0][1]
    latmin = cell[1][0]
    latmax = cell[1][1]
    
    idlon= np.where( (lon >= lonmin) & (lon <= lonmax) )
    idlat= np.where( (lat >= latmin) & (lat <= latmax) )
    
    lon=lon[idlon]
    lat=lat[idlat]
    
    wantedVariable = fh.variables[variable]
    shapeVariable = np.shape(wantedVariable)
    dimVariable = len(shapeVariable)
    if dimVariable==1:
        var=wantedVariable[:]
    if dimVariable==2:
        var=wantedVariable[:,:]
    if dimVariable==3:
        var=wantedVariable[:,:,:]
    if dimVariable==4:
        var=wantedVariable[:,:,:,:]
    
    mat_lonCACATOES = np.zeros((len(lat),len(lon)))
    mat_latCACATOES = np.zeros((len(lat),len(lon)))
  

    for i in range(0,np.shape(mat_lonCACATOES)[0],1): 
        mat_lonCACATOES[i,:]    	= lon
        
    for j in range(0,np.shape(mat_latCACATOES)[1],1):  
        mat_latCACATOES[:,j]   	= lat
        
    fh.close()
    del(fh)
        
    idlatMax = idlat[0][-1] + 1
    idlonMax = idlon[0][-1] + 1
    
    if dimVariable==1:
        subVar=var
    if dimVariable==2:
        subVar=var[idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    if dimVariable==3:
        subVar=var[:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    if dimVariable==4:
        subVar=var[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
    dimensions = np.shape(subVar)
    #on stocke dans le résultat, les données et la dimension 
    subVarResult = (subVar, dimensions)
    return subVarResult
            
def get_CACATOES_SahelDayVariable(dateSahel,variableName, cell):

    year = dateSahel.year
    month = dateSahel.month
    day = dateSahel.day
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    
    veille = dateSahel-datetime.timedelta(days=1)

    valueFromDayBefore = get_CACATOES_DayVariable(veille,variableName, cell, 6, 24)
    valueFromDay = get_CACATOES_DayVariable(dateSahel,variableName, cell, 0, 6)
    
    if valueFromDay[1]==1:
        value = valueFromDayBefore[0] + valueFromDay[0]
    else:
        value = np.concatenate((valueFromDayBefore[0], valueFromDay[0]),axis=0)
   
    #TODO : gérer les doublons : facile pour les tableau avec np.unique, plus chiant pour les variables numériques
    
    return value
            
def indexFractionMCS_OnCellOnMOnth(DAILYmcs_Pop_Month_Cell,QCtoocan_interruption_Month_Cell,
                                   QCmcs_Label_Month_Cell,INT_GridFraction_Month_Cell,
                                   subINIT_TIME,subEND_TIME,subINT_Tmax,subINT_gridtimeOccupation_start,subINT_gridtimeOccupation_end,log=False):
    index_fraction_month = np.zeros(np.shape(DAILYmcs_Pop_Month_Cell)[0])
    for iday in np.arange(0,np.shape(DAILYmcs_Pop_Month_Cell)[0],1):
        # On teste si il y a eu une interruption du tracking à un jour donné
        # si oui on ne traite pas ce jour
        if log==True:
            print('jour =',iday)
            print('population =',DAILYmcs_Pop_Month_Cell[iday,0,0])
            
        ind = np.where((QCtoocan_interruption_Month_Cell[iday,:,:] == 0))
        if(np.shape(ind)[1] == 0):
            print('interruption jour :',iday)
            continue
        sum_fraction_for_day = 0
        
        for imcs in np.arange(0,np.shape(QCmcs_Label_Month_Cell)[1],1):
            if QCmcs_Label_Month_Cell[iday,imcs,0,0] > 0:
                if log==True:
                    print('MCS =',QCmcs_Label_Month_Cell[iday,imcs,0,0])
                    print('Fraction =',INT_GridFraction_Month_Cell[iday,imcs,0,0])
                    print('MCS init =',subINIT_TIME[iday,imcs,0,0])
                    print('MCS end =',subEND_TIME[iday,imcs,0,0])
                    print('MCS Tmax =',subINT_Tmax[iday,imcs,0,0])
                    print('Init MCS in cell =',subINT_gridtimeOccupation_start[iday,imcs,0,0])
                    print('End MCS in cell=',subINT_gridtimeOccupation_end[iday,imcs,0,0])
                dist_from_max = 0
                if subINT_gridtimeOccupation_start[iday,imcs,0,0] > subINT_Tmax[iday,imcs,0,0]:
                    dist = subINT_gridtimeOccupation_start[iday,imcs,0,0] - subINT_Tmax[iday,imcs,0,0]
                if subINT_gridtimeOccupation_end[iday,imcs,0,0] < subINT_Tmax[iday,imcs,0,0]:
                    dist = subINT_Tmax[iday,imcs,0,0] - subINT_gridtimeOccupation_end[iday,imcs,0,0]
                durationMCS = 24* (subEND_TIME[iday,imcs,0,0] - subINIT_TIME[iday,imcs,0,0])
                indexDist = 1 - dist / durationMCS
                if log==True:
                    print('indexDist =',indexDist)
                indexDist = 1
                sum_fraction_for_day = sum_fraction_for_day + indexDist * INT_GridFraction_Month_Cell[iday,imcs,0,0]
                #sum_fraction_for_day = sum_fraction_for_day + 1
        
        index_fraction_month[iday] = sum_fraction_for_day
        
    return index_fraction_month
    
def get_CacatoesYearVariable(variableName, cell, year):

    annualVarData = None
    dimRef = None
    
    for imonth in 1+np.arange(12):
            dirFiles = '../CACATOES/' + str(year) + '/'
            fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(imonth).zfill(2)+'01'+'*'))
            if len(fileCACATOES) > 1:
                raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
         
            fileMonth = fileCACATOES[0]
            varMonthTuple = get_CACATOES_Variable(fileMonth,variableName, cell)
            varData = varMonthTuple[0][:,0,0]
            if imonth == 1:
                dimRef = varMonthTuple[1]
                annualVarData = varMonthTuple[0]
            else:
                if (len(dimRef) != len(varMonthTuple[1])):
                    raise Exception("Les dimensions des données mensuelles ne correspondent pas !")
                annualVarData = np.concatenate((annualVarData, varMonthTuple[0]),axis=0)
    dimensions = np.shape(annualVarData)
    annualVarResult = (annualVarData, dimensions)
    return annualVarResult            
    
def filterDataBetweenHours(dateSahel, valueData, startHour, endHour):
    year = dateSahel.year
    month = dateSahel.month
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    day = dateSahel.day
    INT_gridtimeOccupation_start = fh.variables['INT_gridtimeOccupation_start'][:,:,:,:]
    INT_gridtimeOccupation_end = fh.variables['INT_gridtimeOccupation_end'][:,:,:,:]
    
    
def get_CACATOES_DayVariable(dateCacatoes,variableName, cell, startHour, endHour):
    year = dateCacatoes.year
    month = dateCacatoes.month
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    day = dateCacatoes.day
    dirFiles = '../CACATOES/' + str(year) + '/'
    fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(month).zfill(2)+'01'+'*'))
    if len(fileCACATOES) > 1:
        raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
    fileMonth = fileCACATOES[0]
    varMonthTuple = get_CACATOES_Variable(fileMonth, variableName, cell)
    startInCellTuple = get_CACATOES_Variable(fileMonth,'INT_gridtimeOccupation_start', cell)
    endInCellTuple = get_CACATOES_Variable(fileMonth,'INT_gridtimeOccupation_end', cell)
    startInCell_day = startInCellTuple[0][day-1,:,0,0]
    endInCell_day = endInCellTuple[0][day-1,:,0,0]
    dim = None
    if len(varMonthTuple[1])==3:
        dim = 1
        valueData = varMonthTuple[0][day-1,0,0]
        length = valueData
        for i in np.arange(length):
            if (startInCell_day[i] > endHour or endInCell_day[i] < startHour):
                valueData = valueData - 1
    if len(varMonthTuple[1])==4:
        dim = 2
        valueData = varMonthTuple[0][day-1,:,0,0]
        length = len(np.where( valueData != 0 ))
        it = np.where( valueData != 0 )[0]
        for i in it:
            drop = (startInCell_day[i] > endHour or endInCell_day[i] < startHour)
            if drop == True:  
                valueData[i] = 0
        idNonZero= np.where( valueData != 0 )
        valueData = valueData[idNonZero]
    result = (valueData, dim)    
    return result
    
def loadCacatoesFilesForYear(directoryFiles, year):
    cacatoesDictionary = {}
    for imonth in (np.arange(12) + 1):
        
        dirFiles = directoryFiles + str(year) + '/'
        fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(imonth).zfill(2)+'01'+'*'))
        if len(fileCACATOES) > 1:
            raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
        fileMonth = fileCACATOES[0]
        fh = netCDF4.Dataset(fileMonth,'r',diskless=True)
        cacatoesDictionary[imonth] = fh
    return cacatoesDictionary
    
def get_CACATOES_SahelDayForListVariables(dateSahel,variablesDictionary, cell, needCheckVeille = True,pathFilesCacatoes = '../CACATOES/'):

    year = dateSahel.year
    month = dateSahel.month
    day = dateSahel.day
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    veille = dateSahel-datetime.timedelta(days=1)
    resultValues = {}
    if needCheckVeille == True:
        listValuesFromDayBefore = get_CACATOES_DayForListVariables(veille,variablesDictionary, cell, 6, 24, True, pathFilesCacatoes)
        listValueFromDay = get_CACATOES_DayForListVariables(dateSahel,variablesDictionary, cell, 0, 6, False, pathFilesCacatoes)
        for var in np.arange(len(variablesDictionary)):
            varName = variablesDictionary[var]
            if listValueFromDay[varName][1]==1:
                resultValues[varName] = listValuesFromDayBefore[varName][0] + listValueFromDay[varName][0]
            else:
                resultValues[varName] = np.concatenate((listValuesFromDayBefore[varName][0], listValueFromDay[varName][0]),axis=0)
            
        if listValueFromDay['PREV_SAHEL_DAY'][1]==1:
            resultValues['PREV_SAHEL_DAY'] = listValuesFromDayBefore['PREV_SAHEL_DAY'][0] + listValueFromDay['PREV_SAHEL_DAY'][0]
        else:
            resultValues['PREV_SAHEL_DAY'] = np.concatenate((listValuesFromDayBefore['PREV_SAHEL_DAY'][0], listValueFromDay['PREV_SAHEL_DAY'][0]),axis=0)
    else:
        listValueFromDay = get_CACATOES_DayForListVariables(dateSahel,variablesDictionary, cell, 0, 24, False, pathFilesCacatoes)
        for var in np.arange(len(variablesDictionary)):
            varName = variablesDictionary[var]
            if listValueFromDay[varName][1]==1:
                resultValues[varName] = listValueFromDay[varName][0]
            else:
                resultValues[varName] = listValueFromDay[varName][0]
        if listValueFromDay['PREV_SAHEL_DAY'][1]==1:
            resultValues['PREV_SAHEL_DAY'] = listValueFromDay['PREV_SAHEL_DAY'][0]
        else:
            resultValues['PREV_SAHEL_DAY'] = listValueFromDay['PREV_SAHEL_DAY'][0]
    #resultValues['PREV_SAHEL_DAY'] = np.concatenate((listValuesFromDayBefore['PREV_SAHEL_DAY'][0], listValueFromDay['PREV_SAHEL_DAY'][0]),axis=0)
    #TODO : gérer les doublons : facile pour les tableau avec np.unique, plus chiant pour les variables numériques
    
    return resultValues
    
def get_CACATOES_DayForListVariables(dateCacatoes,variablesDictionary, cell, startHour, endHour, prevSahelDay, pathFilesCacatoes = '../CACATOES/'):
    year = dateCacatoes.year
    month = dateCacatoes.month
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    day = dateCacatoes.day
    dirFiles = pathFilesCacatoes + str(year) + '/'
    fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(month).zfill(2)+'01'+'*'))
    if len(fileCACATOES) > 1:
        raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
    if len(fileCACATOES) == 0:
        raise Exception("Données manquantes pour la date :" + str(dateCacatoes))
    fileMonth = fileCACATOES[0]
    nbWantedVariables = len(variablesDictionary)
    nbCurVar = nbWantedVariables
    
    if ('INT_gridtimeOccupation_start' in variablesDictionary.values()) == False:
        
        variablesDictionary[nbCurVar] = 'INT_gridtimeOccupation_start'
        nbCurVar = nbCurVar+1
    if ('INT_gridtimeOccupation_end' in variablesDictionary.values()) == False:
        
        variablesDictionary[nbCurVar] = 'INT_gridtimeOccupation_end'
        nbCurVar = nbCurVar+1
    
    resultVariables = get_CACATOES_ListVariables(fileMonth,variablesDictionary, cell)
    
    #varMonthTuple = get_CACATOES_Variable(fileMonth, variableName, cell)
    #startInCellTuple = get_CACATOES_Variable(fileMonth,'INT_gridtimeOccupation_start', cell)
    #endInCellTuple = get_CACATOES_Variable(fileMonth,'INT_gridtimeOccupation_end', cell)
    startInCell_day = resultVariables['INT_gridtimeOccupation_start'][0][day-1,:,0,0]
    endInCell_day = resultVariables['INT_gridtimeOccupation_end'][0][day-1,:,0,0]
    
    nbMCSOnDay = 0
    for var in np.arange(nbWantedVariables):
        variableName = variablesDictionary[var]
        variableResult = resultVariables[variableName]
        dim = None
        if len(variableResult[1])==3:
            dim = 1
            valueData = variableResult[0][day-1,0,0]
            
            length = valueData
            if variableName == 'DAILYmcs_Pop':
                nbMCSOnDay = length
            for i in np.arange(length):
                if (startInCell_day[i] > endHour or endInCell_day[i] < startHour):
                    valueData = valueData - 1
            #infoDay = prevSahelDay
            infoDay = np.empty(1)
            infoDay[0] = prevSahelDay
        if len(variableResult[1])==4:
            
            dim = 2
            valueData = variableResult[0][day-1,0:nbMCSOnDay,0,0]
            
            keepIds = []
            #it = np.where( valueData != 0 )[0]
            
            for i in np.arange(nbMCSOnDay):
                drop = (startInCell_day[i] > endHour or endInCell_day[i] < startHour)
                
                if drop == False:  
                    keepIds.append(i)
            keepTab = np.array(keepIds)        
            #idOK = np.where( valueData != invalidValue )
            
            if len(keepTab) > 0:
                valueData = valueData[keepTab]
                infoDay = np.empty(len(valueData))
                for i in np.arange(len(valueData)):
                    infoDay[i] = prevSahelDay
            else:
                valueData = np.zeros(0)
                infoDay = np.zeros(0)
            
        #result = (valueData, dim)    
        resultVariables[variableName] = (valueData, dim)
    resultVariables['PREV_SAHEL_DAY'] = (infoDay, 2)
            
    while nbCurVar > nbWantedVariables:
        varToDelete = variablesDictionary[nbCurVar-1]
        del resultVariables[varToDelete]
        del variablesDictionary[nbCurVar-1] 
        nbCurVar = nbCurVar-1
    
    return resultVariables
    
def get_CACATOES_ListVariables(file,variablesDictionary, cell):

    fh = netCDF4.Dataset(file,'r')
    
    lon = fh.variables['lon'][:]
    lat = fh.variables['lat'][:]
    
    lonmin = cell[0][0]
    lonmax = cell[0][1]
    latmin = cell[1][0]
    latmax = cell[1][1]
    
    idlon= np.where( (lon >= lonmin) & (lon <= lonmax) )
    idlat= np.where( (lat >= latmin) & (lat <= latmax) )
    
    lon=lon[idlon]
    lat=lat[idlat]
    resultDictionary = {}
    for var in np.arange(len(variablesDictionary)):
        variable = variablesDictionary[var]
        wantedVariable = fh.variables[variable]
        shapeVariable = np.shape(wantedVariable)
        dimVariable = len(shapeVariable)
        if dimVariable==1:
            var=wantedVariable[:]
        if dimVariable==2:
            var=wantedVariable[:,:]
        if dimVariable==3:
            var=wantedVariable[:,:,:]
        if dimVariable==4:
            var=wantedVariable[:,:,:,:]
        
        mat_lonCACATOES = np.zeros((len(lat),len(lon)))
        mat_latCACATOES = np.zeros((len(lat),len(lon)))
      

        for i in range(0,np.shape(mat_lonCACATOES)[0],1): 
            mat_lonCACATOES[i,:]    	= lon
            
        for j in range(0,np.shape(mat_latCACATOES)[1],1):  
            mat_latCACATOES[:,j]   	= lat
            
        idlatMax = idlat[0][-1] + 1
        idlonMax = idlon[0][-1] + 1
                
        if dimVariable==1:
            subVar=var
        if dimVariable==2:
            subVar=var[idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
        if dimVariable==3:
            subVar=var[:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
        if dimVariable==4:
            subVar=var[:,:,idlat[0][0]:idlatMax,idlon[0][0]:idlonMax]
        dimensions = np.shape(subVar)
        #on stocke dans le résultat, les données et la dimension 
        subVarResult = (subVar, dimensions)
        resultDictionary[variable] = subVarResult
    fh.close()
    del(fh)
    return resultDictionary
    
def getDistToTmax(day, gridStart,gridEnd, initMCS, endMCS, TmaxMCS, prevSahelDay, needCheckVeille = True):

    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    sahelveillestamp = saheldaystamp - 1
    
    MCSduration = endMCS - initMCS
    tmax = initMCS + 0.01 * TmaxMCS * MCSduration
    distToMax = 0
    startStampInCellIfDay = saheldaystamp + gridStart / 24
    endStampInCellIfDay = saheldaystamp + gridEnd / 24
    #on prend un peu de marge car les valeurs de gridStart et gridEnd sont arrondies
    #TODO :  à améliorer !!
    initMCSmarged = initMCS - 0.05 #environ 1 heure de marge
    endMCSmarged = endMCS + 0.05 #environ 1 heure de marge
    startStampInCellIfVeille = sahelveillestamp + gridStart / 24
    endStampInCellIfVeille = sahelveillestamp + gridEnd / 24
    if needCheckVeille == False:
        startStamp = startStampInCellIfDay
        endStamp = endStampInCellIfDay
    else:
        DayPossible = (startStampInCellIfDay >= initMCSmarged and endStampInCellIfDay <= endMCSmarged)
        VeillePossible = (startStampInCellIfVeille >= initMCSmarged and endStampInCellIfVeille <= endMCSmarged)
        if (DayPossible == True and VeillePossible == False):
            startStamp = startStampInCellIfDay
            endStamp = endStampInCellIfDay
        elif (DayPossible == False and VeillePossible == True):
            startStamp = startStampInCellIfVeille
            endStamp = endStampInCellIfVeille
        elif (DayPossible == True and VeillePossible == True):
            if gridStart > 6:
                startStamp = startStampInCellIfVeille
                endStamp = endStampInCellIfVeille
            elif gridEnd < 6:
                startStamp = startStampInCellIfDay
                endStamp = endStampInCellIfDay
            else:
                #choix par défaut
                startStamp = startStampInCellIfVeille
                endStamp = endStampInCellIfVeille
        else:
            print('La date MCS ne correspond pas !',day,'initMCS=',initMCS,'endMCS=',endMCS,'gridStart=',gridStart,'gridEnd=',gridEnd,'TmaxMCS=',TmaxMCS,'prevSahelDay=',prevSahelDay, 'saheldaystamp=', saheldaystamp)
            startStamp = tmax + 0.5
            endStamp = tmax - 0.5
       
    if startStamp > tmax:
        distToMax = 24*(startStamp - tmax)
    if endStamp < tmax:
        distToMax = 24*(tmax - endStamp)
    return distToMax
    
def getRealGridTime(gridStartTab,gridEndTab, prevSahelDayTab, transitionHour = 6, needCheckVeille = True):
    if needCheckVeille == False:
        gridTimeResult = gridEndTab - gridStartTab
    else:
        gridTimeResult = np.zeros(len(gridStartTab))
        for i in np.arange(len(gridStartTab)):
            gridStart = gridStartTab[i]
            gridEnd = gridEndTab[i]
            dayBefore = prevSahelDayTab[i]
            if dayBefore == True:
                gridStart = np.maximum(transitionHour, gridStart)
            else:
                gridEnd = np.minimum(transitionHour, gridEnd)
            gridTimeResult[i] = gridEnd - gridStart
    return gridTimeResult
    
def getGridTimeBeforeTmax(day, gridStartTab,gridEndTab, TmaxTab, initMCSTab, endMCSTab):
    length = len(gridStartTab)
    gridTimeResult = np.empty(length)
    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    
    MCSduration = endMCSTab - initMCSTab
    tmax = initMCSTab + 0.01 * TmaxTab * MCSduration
    
    startStampInCell = saheldaystamp + gridStartTab / 24
    endStampInCell = saheldaystamp + gridEndTab / 24
    
    for m in np.arange(length):
        if startStampInCell[m] > tmax[m]:
            gridTimeResult[m] = 0
        elif endStampInCell[m] > tmax[m]:
            gridTimeResult[m] = 24 * (tmax[m] - startStampInCell[m])
        else:
            gridTimeResult[m] = 24 * (endStampInCell[m] - startStampInCell[m])
    return gridTimeResult

def getMCSDurationBeforeTmax(day, TmaxTab, initMCSTab, endMCSTab):
    length = len(initMCSTab)
    timeResult = np.empty(length)
    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    
    MCSduration = endMCSTab - initMCSTab
    tmax = initMCSTab + 0.01 * TmaxTab * MCSduration
    
    timeResult = 24* (tmax - initMCSTab)
    return timeResult
    
def getMCSDurationAfterTmax(day, TmaxTab, initMCSTab, endMCSTab):
    length = len(initMCSTab)
    timeResult = np.empty(length)
    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    
    MCSduration = endMCSTab - initMCSTab
    tmax = initMCSTab + 0.01 * TmaxTab * MCSduration
    
    timeResult = 24 * (endMCSTab - tmax)
    return timeResult
    
def GetPointsOnPathBeforeTmax(day, gridStart,gridEnd, Tmax, initMCS, endMCS, xInitMCS, yInitMCS, xEndMCS, yEndMCS, xTmaxMCS, yTmaxMCS):
    gridTimeResult = 0
    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    
    MCSduration = endMCS - initMCS
    tmax = initMCS + 0.01 * Tmax * MCSduration
    
    startStampInCell = saheldaystamp + gridStart / 24
    endStampInCell = saheldaystamp + gridEnd / 24
    
    if startStampInCell > tmax:
        gridTimeResult = 0
    elif endStampInCell > tmax:
        gridTimeResult = 24 * (tmax - startStampInCell)
    else:
        gridTimeResult = 24 * (endStampInCell - startStampInCell)
    
    if endStampInCell < tmax:
        timeInGridAfterTmax = 0
    elif startStampInCell < tmax:
        timeInGridAfterTmax = 24 * (endStampInCell - tmax)
    else:
        timeInGridAfterTmax = 24 * (endStampInCell - startStampInCell)
    
    #print('init MCS pos = ', xInitMCS, yInitMCS)
    #print('Tmax MCS pos = ', xTmaxMCS, yTmaxMCS)    
    #print('Start in cell = ', startStampInCell, 'Time in cell before Tmax',gridTimeResult)
    #print('init MCS time = ', initMCS, 'Tmax mcs time', tmax,'Tmax%=',Tmax)
    
    res = getPosListBeforeTmax(xInitMCS, yInitMCS, initMCS, startStampInCell, gridTimeResult, xTmaxMCS, yTmaxMCS, tmax)
    #print('result=',res)
    
    return res

def GetPointsOnPathAfterTmax(day, gridStart,gridEnd, Tmax, initMCS, endMCS, xInitMCS, yInitMCS, xEndMCS, yEndMCS, xTmaxMCS, yTmaxMCS):
    gridTimeResult = 0
    current_timestamp = calendar.timegm(datetime.datetime.utctimetuple(day))
    current_timestamp = current_timestamp / (24*3600)
    saheldaystamp = current_timestamp - 0.25
    
    MCSduration = endMCS - initMCS
    tmax = initMCS + 0.01 * Tmax * MCSduration
    
    startStampInCell = saheldaystamp + gridStart / 24
    endStampInCell = saheldaystamp + gridEnd / 24
    
    if startStampInCell > tmax:
        gridTimeResult = 0
    elif endStampInCell > tmax:
        gridTimeResult = 24 * (tmax - startStampInCell)
    else:
        gridTimeResult = 24 * (endStampInCell - startStampInCell)
    
    if endStampInCell < tmax:
        timeInGridAfterTmax = 0
    elif startStampInCell < tmax:
        timeInGridAfterTmax = 24 * (endStampInCell - tmax)
    else:
        timeInGridAfterTmax = 24 * (endStampInCell - startStampInCell)
    
    res = getPosListAfterTmax(endStampInCell, timeInGridAfterTmax, xTmaxMCS, yTmaxMCS, tmax, xEndMCS, yEndMCS, endMCS)
    
    return res
    
def getPosListBeforeTmax(xStart, yStart, tStart, tStartInGrid, timeInGridBeforeTmax, xTmax, yTmax, Tmax):
    if timeInGridBeforeTmax == 0:
        return (np.empty(0), np.empty(0))
    distX = xTmax - xStart
    distY = yTmax - yStart
    H = 24* (Tmax - tStart)
    
    if H==0:
        return (np.empty(0), np.empty(0))
    dXi = distX / H
    dYi = distY / H
    nStep = int(np.around(timeInGridBeforeTmax)) + 1
    xPos = np.empty(nStep)
    yPos = np.empty(nStep)
    offset = 24 * (tStartInGrid - tStart)
    for i in np.arange(nStep):
        xPos[i] = xStart + (offset + i) * dXi
        yPos[i] = yStart + (offset + i) * dYi
    return xPos, yPos
    
def getPosListAfterTmax(tEndInGrid, timeInGridAfterTmax, xTmax, yTmax, Tmax, xEnd, yEnd, tEnd):
    if timeInGridAfterTmax == 0:
        return (np.empty(0), np.empty(0))
    distX = xEnd - xTmax
    distY = yEnd - yTmax
    H = 24* (tEnd - Tmax)
    if H==0:
        return (np.empty(0), np.empty(0))
    dXi = distX / H
    dYi = distY / H
    nStep = int(np.around(timeInGridAfterTmax)) + 1
    xPos = np.empty(nStep)
    yPos = np.empty(nStep)
    for i in np.arange(nStep):
        xPos[i] = xTmax + i * dXi
        yPos[i] = yTmax + i * dYi
    return xPos, yPos
#def comparePluieSahelEtCacatoesData(villes, annees, parametres, seuils):



def get_CACATOES_MultiDayForListVariables(datesCacatoes,variablesDictionary, cell, startHour, endHour, prevSahelDay):
    year = datesCacatoes[0].year
    month = datesCacatoes[0].month
    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    
    nbDay = len(datesCacatoes)
    days = np.zeros(nbDay,dtype=int)
    i=0
    for date in datesCacatoes:
        days[i] = date.day
        i+=1
    days = days - 1
    
    dirFiles = '../CACATOES/' + str(year) + '/'
    fileCACATOES = sorted(glob.glob(dirFiles+'*'+str(year)+str(month).zfill(2)+'01'+'*'))
    if len(fileCACATOES) > 1:
        raise Exception("plusieurs choix de fichier possibles dans get_CacatoesYearVariable !")
    if len(fileCACATOES) == 0:
        raise Exception("Données manquantes pour la date :" + str(datesCacatoes))
    fileMonth = fileCACATOES[0]
    nbWantedVariables = len(variablesDictionary)
    nbCurVar = nbWantedVariables
    
    if ('INT_gridtimeOccupation_start' in variablesDictionary.values()) == False:      
        variablesDictionary[nbCurVar] = 'INT_gridtimeOccupation_start'
        nbCurVar = nbCurVar+1
    if ('INT_gridtimeOccupation_end' in variablesDictionary.values()) == False:
        
        variablesDictionary[nbCurVar] = 'INT_gridtimeOccupation_end'
        nbCurVar = nbCurVar+1
    
    resultVariables = get_CACATOES_ListVariables(fileMonth,variablesDictionary, cell)
    
    startInCell_day = resultVariables['INT_gridtimeOccupation_start'][0][:,:,0,0]
    endInCell_day = resultVariables['INT_gridtimeOccupation_end'][0][:,:,0,0]
    resultToReturn = {}
    resultToReturn['PREV_SAHEL_DAY'] = {}
    compute_Prev_Sahel_Day = False
    nbMCSForDays = np.empty(len(days),dtype=int)
    for var in np.arange(nbWantedVariables):
        variableName = variablesDictionary[var]
        variableResult = resultVariables[variableName]
        resultToReturn[variableName] = variableResult[0][days,0,0]
        #print('variable = ',variableName, 'result=',resultToReturn[variableName])
        #valueDataForDays = np.empty(len(days))
        valueDataForDays = {}
        jday = 0
        for day in days:       
            
            dim = None
            if len(variableResult[1])==3:
                dim = 1
                valueData = variableResult[0][day,0,0]
                
                length = valueData
                if variableName == 'DAILYmcs_Pop':
                    nbMCSForDays[jday] = length
                
                for i in np.arange(length):
                    
                    if (startInCell_day[day][i] > endHour or endInCell_day[day][i] < startHour):
                        valueData = valueData - 1
               
            if len(variableResult[1])==4:
                compute_Prev_Sahel_Day = True
                nbMCSOnDay = nbMCSForDays[jday]
                
                dim = 2
                valueData = variableResult[0][day,0:nbMCSOnDay,0,0]
                
                keepIds = []             
                
                for i in np.arange(nbMCSOnDay):
                    drop = (startInCell_day[day][i] > endHour or endInCell_day[day][i] < startHour)
                    
                    if drop == False:  
                        keepIds.append(i)
                keepTab = np.array(keepIds)                      
                
                if len(keepTab) > 0:
                    valueData = valueData[keepTab]
                    if compute_Prev_Sahel_Day == True:
                        prev_Sahel_Day_values = np.full(len(keepTab),prevSahelDay)
                else:
                    valueData = np.zeros(0)
                    if compute_Prev_Sahel_Day == True:
                        prev_Sahel_Day_values = np.zeros(0)
                    
            valueDataForDays[jday] = valueData
            if compute_Prev_Sahel_Day == True:
                resultToReturn['PREV_SAHEL_DAY'][jday] = prev_Sahel_Day_values
            jday += 1
        resultToReturn[variableName] = valueDataForDays
        if compute_Prev_Sahel_Day == True:
            compute_Prev_Sahel_Day = False
            
    while nbCurVar > nbWantedVariables:
        varToDelete = variablesDictionary[nbCurVar-1]
        del resultVariables[varToDelete]
        del variablesDictionary[nbCurVar-1] 
        nbCurVar = nbCurVar-1
    
    return resultToReturn
    
def get_CACATOES_SahelMultiDayForListVariables(datesSahel,variablesDictionary, cell):

    #attention les dates de la base de données sahel sont données à 6h du matin
    #les quantités de pluie associées à ces dates correspondent donc au cumul de pluie tombé depuis la veille à 6h du matin.
    #on considère que les jours de cacatoes sont donnés de 0h à 0h
    #un cumul de pluie sahel est donc à mettre en relation avec:
    #       - les données cacatoes de la veille de 6h à 24     
    #       - les données cacatoes du jour de 0h à 6h
    nbJours = len(datesSahel)
    if nbJours == 0:
        resultValues = {}
        for var in np.arange(len(variablesDictionary)):
            varName = variablesDictionary[var]
            resultValues[varName] = {}
        resultValues['PREV_SAHEL_DAY'] = {}
        resultValues['DAY_FOR_MONTH'] = {}
        return resultValues
        
    veillesDates = np.zeros(nbJours,dtype=object)
    jday = 0
    for day in datesSahel:
        d = datesSahel[jday]-datetime.timedelta(days=1)
        veillesDates[jday] = d
        jday += 1
    
    resultValues = {}
    resultValues['DAY_FOR_MONTH'] = {}
    decomposePrevDays = False
    if nbJours > 1:
        firstDay = veillesDates[0]
        secondDay = veillesDates[1]
        if firstDay.month != secondDay.month:
            decomposePrevDays = True
            
    if decomposePrevDays==True:
        listValuesFromDayBefore = {}
        firstDayDates = np.zeros(1,dtype=object)
        firstDayDates[0] = firstDay
        listValuesForFirstDay = get_CACATOES_MultiDayForListVariables(firstDayDates,variablesDictionary, cell, 6, 24, True)
        realVeillesDates = np.zeros(nbJours - 1,dtype=object)
        for i in np.arange(len(realVeillesDates)):
            realVeillesDates[i] = veillesDates[i+1]
        listValuesForRealVeille = get_CACATOES_MultiDayForListVariables(realVeillesDates,variablesDictionary, cell, 6, 24, True)
        for var in np.arange(len(variablesDictionary)):
            varName = variablesDictionary[var]
            listValuesFromDayBefore[varName] = {}
            
            listValuesFromDayBefore[varName][0] = listValuesForFirstDay[varName][0]
            for i in np.arange(len(realVeillesDates)):
                listValuesFromDayBefore[varName][i+1] = listValuesForRealVeille[varName][i]
        listValuesFromDayBefore['PREV_SAHEL_DAY'] = {}
        listValuesFromDayBefore['PREV_SAHEL_DAY'][0] = listValuesForFirstDay['PREV_SAHEL_DAY'][0]
        for i in np.arange(len(realVeillesDates)):
            listValuesFromDayBefore['PREV_SAHEL_DAY'][i+1] = listValuesForRealVeille['PREV_SAHEL_DAY'][i]
    else:
        listValuesFromDayBefore = get_CACATOES_MultiDayForListVariables(veillesDates,variablesDictionary, cell, 6, 24, True)
        
        
    listValueFromDay = get_CACATOES_MultiDayForListVariables(datesSahel,variablesDictionary, cell, 0, 6, False)
    resultValues['PREV_SAHEL_DAY'] = {}
    compute_prev_sahel_day = True
    for var in np.arange(len(variablesDictionary)):
        varName = variablesDictionary[var]
        resultValues[varName] = {}
        jday = 0
        for day in datesSahel:
        
            if varName=='DAILYmcs_Pop':
                resultValues[varName][jday] = listValuesFromDayBefore[varName][jday] + listValueFromDay[varName][jday]
            else:
                resultValues[varName][jday] = np.concatenate((listValuesFromDayBefore[varName][jday], listValueFromDay[varName][jday]),axis=0)
            if compute_prev_sahel_day == True:
                resultValues['PREV_SAHEL_DAY'][jday] = np.concatenate((listValuesFromDayBefore['PREV_SAHEL_DAY'][jday], listValueFromDay['PREV_SAHEL_DAY'][jday]),axis=0)
                resultValues['DAY_FOR_MONTH'][jday] = day
            jday += 1
        compute_prev_sahel_day = False    
            
    #TODO : gérer les doublons : facile pour les tableau avec np.unique, plus chiant pour les variables numériques
    
    return resultValues
    
def ComputePlotData_Sahel_Cacatoes(DailyRainSahel, villesDictionary, cells, cacatoesVarDict, wantedIndicesDict, seuilPluvio, pluvioMax, listAnneesDict, progress, needCheckVeille = True, nbMaxMcs = 25, nbMCSToTakeInIndice = 25, minStd = 0.0, maxStd = 1000.0, givePrevAndNextDay = False, seuilWithMoyenne=True, pathFilesCacatoes = '../CACATOES/'):
    dataPluie = np.empty(0)
    villeSerieTotale = np.empty(0)
    print('seuilPluvio=',seuilPluvio)
    
    CheckDateOnMaxDoCalculationOnMean = (len(villesDictionary) == 3 and villesDictionary[0] == 'moyenne' and villesDictionary[1] == 'max' and villesDictionary[2] == 'std')
    print('CheckDateOnMaxDoCalculationOnMean=',CheckDateOnMaxDoCalculationOnMean)
    for ville in villesDictionary.values():
        print('ville:',ville)
        if CheckDateOnMaxDoCalculationOnMean == True and ville != 'max':
            continue
        dataPluieVille = np.empty(0)
        for annee in listAnneesDict.values():
            if CheckDateOnMaxDoCalculationOnMean == True:
                if seuilWithMoyenne == True:
                    dataPluieAnnee_moyenne, nbValue = PluvioSahelUtils.getDateOverThresholdRainSimple(DailyRainSahel, 'moyenne', annee, seuilPluvio,pluvioMax)
                    dataPluieAnnee_max = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, 'max', annee, givePrevAndNextDay)
                    dataPluieAnnee_std = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, 'std', annee, givePrevAndNextDay)
                    dataPluieAnnee_moyenne = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, 'moyenne', annee, False)            
                else:
                    dataPluieAnnee_max = PluvioSahelUtils.getDateOverThresholdRainSimple(DailyRainSahel, 'max', annee, seuilPluvio,pluvioMax)
                    dataPluieAnnee_moyenne = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_max.index, 'moyenne', annee, givePrevAndNextDay)
                    dataPluieAnnee_std = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_max.index, 'std', annee, givePrevAndNextDay)
                    dataPluieAnnee_max = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, 'max', annee, False)
                    
                
            else:
                dataPluieAnnee_moyenne, nbMesure = PluvioSahelUtils.getDateOverThresholdRainSimple(DailyRainSahel, ville, annee, seuilPluvio,pluvioMax)
                dataPluieAnnee_max = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, ville, annee, False)
                dataPluieAnnee_std = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, ville, annee, False)
                dataPluieAnnee_moyenne = PluvioSahelUtils.getValuesForListDates(DailyRainSahel, dataPluieAnnee_moyenne.index, ville, annee, False)
                #print('Nombre de mesure pour la ville : ',ville,' et année : ',annee,' = ',nbMesure)
            
            date_serie = pd.Series(dataPluieAnnee_max.index)
            date_serie.name = 'date'
            dataPluieAnnee_max.index = date_serie.index
            dataPluieAnnee_max.name = 'pluie max'
            dataPluieAnnee_moyenne.index = date_serie.index
            dataPluieAnnee_moyenne.name = 'pluie moyenne'
            dataPluieAnnee_std.index = date_serie.index
            dataPluieAnnee_std.name = 'pluie std'
            dataPluieAnnee = pd.concat([date_serie,dataPluieAnnee_max, dataPluieAnnee_moyenne, dataPluieAnnee_std], axis=1)
            nbDataForVille = len(dataPluieVille)
            if nbDataForVille == 0:
                dataPluieVille = dataPluieAnnee
            else:
                dataPluieVille = pd.concat([dataPluieVille, dataPluieAnnee])
        if CheckDateOnMaxDoCalculationOnMean == True:
            villeSerie = pd.Series(np.full(len(dataPluieVille), 'moyenne'),index=dataPluieVille.index, copy=False)
        else:
            villeSerie = pd.Series(np.full(len(dataPluieVille), ville),index=dataPluieVille.index, copy=False)
        nbData = len(dataPluie)
        
        
        if nbData==0:
            dataPluie = dataPluieVille
            villeSerieTotale = villeSerie
            
        else:
            dataPluie = pd.concat([dataPluie, dataPluieVille])
            villeSerieTotale = pd.concat([villeSerieTotale, villeSerie])
            
        #dataPluie.name = 'Pluie'
    villeSerieTotale.name = 'Ville'
    
    dataPluieWithVille = pd.concat([dataPluie, villeSerieTotale], axis=1)
    dataPluieWithVille.index = np.arange(len(dataPluieWithVille))
    #print('dataPluieWithVille=')
    
    
    print('nombre de jours supérieurs au seuil:',len(dataPluieWithVille))
    
    tempDictNbMCS = {}
    tempDictNbMCS[0] = 'DAILYmcs_Pop'
    
    if len(wantedIndicesDict) > 0:
        print('suppression des jours au delà du seuil MaxMcs: ',nbMaxMcs,' ou sans aucun MCS')
        for i in dataPluieWithVille.index:
            ville = dataPluieWithVille['Ville'][i]
            
            day = dataPluieWithVille['date'][i]
            resultNbMCS = get_CACATOES_SahelDayForListVariables(day,tempDictNbMCS, cells[ville],needCheckVeille, pathFilesCacatoes)
            nbMCSForDay = resultNbMCS['DAILYmcs_Pop']
            suppressDayWithoutMCS = (seuilPluvio > 0.0 and nbMCSForDay == 0)
            if nbMCSForDay > nbMaxMcs or suppressDayWithoutMCS == True:
                print('suppression du jour :',day,'NbMCS = ',nbMCSForDay, 'Pluie = ',  dataPluieWithVille['pluie max'][i])
                dataPluieWithVille = dataPluieWithVille.drop(i)
        dataPluieWithVille.index = np.arange(len(dataPluieWithVille))
        print('nombre de jours supérieurs au seuil:',len(dataPluieWithVille))   
    
    if maxStd < 1000.0 or minStd > 0.0:
        
        print('suppression des jours en dehors des seuils std: ',minStd, maxStd)
        for i in dataPluieWithVille.index:
            valuemoyenne = dataPluieWithVille['pluie moyenne'][i]
            valuestd = dataPluieWithVille['pluie std'][i]
            
            if valuestd / valuemoyenne > maxStd:
                dataPluieWithVille = dataPluieWithVille.drop(i)
            elif valuestd / valuemoyenne < minStd:
                dataPluieWithVille = dataPluieWithVille.drop(i)
        dataPluieWithVille.index = np.arange(len(dataPluieWithVille))
        print('nombre de jours supérieurs au seuil:',len(dataPluieWithVille))   
    
    indicesBuiltDict = {}
    for indice in wantedIndicesDict.values():
        indicesBuiltDict[indice] = np.zeros(dataPluieWithVille.index.size)
    listEpisod = {}
    MaxValues = {}
    
    if len(wantedIndicesDict) == 0:
        return dataPluieWithVille, indicesBuiltDict, MaxValues, listEpisod   
    
    nbDay = len(dataPluieWithVille.index)
    prevVille = ''
    prevYear = 0
    for i in dataPluieWithVille.index:
        day = dataPluieWithVille['date'][i]
        year = day.year
        if CheckDateOnMaxDoCalculationOnMean == True and year != prevYear:
            print("calcul des indices pour l'année : ", year,' ...')
            #print('day=',day, 'pluie moyenne=',dataPluieWithVille['pluie moyenne'][i], 'pluie max=',dataPluieWithVille['pluie max'][i], 'pluie std=',dataPluieWithVille['pluie std'][i])
        ville = dataPluieWithVille['Ville'][i]
        if CheckDateOnMaxDoCalculationOnMean == False and ville != prevVille:
            print('resultats pour ville : ',ville)
        prevVille = ville
        prevYear = year
        resultVariables = get_CACATOES_SahelDayForListVariables(day,cacatoesVarDict, cells[ville],needCheckVeille, pathFilesCacatoes)
        nbMCSForDay = resultVariables['DAILYmcs_Pop']
        if nbMCSForDay == 0:
            print('Pas de mcs ce jour, on passe...')
            continue
        if dataPluieWithVille['pluie moyenne'][i] > 1.0:
            listEpisod[day] = {}            
            for mcs in np.arange(len(resultVariables['QCmcs_Label'])):
                listEpisod[day][mcs] = resultVariables['QCmcs_Label'][mcs]
        #for mcs in np.arange(len(resultVariables['QCmcs_Label'])):
        #    print('       mcs = ',resultVariables['QCmcs_Label'][mcs], 'start mcs =', resultVariables['INIT_Time'][mcs])
        #    print('       interval in grid = ',resultVariables['INT_gridtimeOccupation_start'][mcs],';',resultVariables['INT_gridtimeOccupation_end'][mcs])
        for indice in wantedIndicesDict.values():
            resForIndice = getIndiceFromCacatoesVariable(indice, resultVariables, day, needCheckVeille, nbMCSToTakeInIndice)
            indicesBuiltDict[indice][i] = resForIndice
        if progress != None:
            progress.value = 100 * i / nbDay
    
    
    for indice in wantedIndicesDict.values():
        MaxValues[indice] = 1.1 * np.max(indicesBuiltDict[indice])
    
    return dataPluieWithVille, indicesBuiltDict, MaxValues, listEpisod
    
    
def getIndiceFromCacatoesVariable(wantedIndice, resCacatoesTab, day, needCheckVeille = True, nbMCSToTake = 25):
    result = 0
    lengthTab = len(resCacatoesTab['INT_GridFraction_235K'])
    nbVal = np.minimum(nbMCSToTake, lengthTab)
    if wantedIndice == 'Fraction cell index 235K':
        result = np.sum(resCacatoesTab['INT_GridFraction_235K'][0:nbVal])
    elif wantedIndice == 'Fraction cell index 200K':
        result = np.sum(resCacatoesTab['INT_GridFraction_200K'][0:nbVal])
    elif wantedIndice == 'Fraction cell index 210K':
        result = np.sum(resCacatoesTab['INT_GridFraction_210K'][0:nbVal])
    elif wantedIndice == 'Distance MCS index':
        result = np.sum(resCacatoesTab['INT_Distance'][0:nbVal])
    elif wantedIndice == 'Smax 235 index':
        result = np.sum(resCacatoesTab['INT_Smax_235K'][0:nbVal])
    elif wantedIndice == 'Smax 210 index':
        result = np.sum(resCacatoesTab['INT_Smax_210K'][0:nbVal])
    elif wantedIndice == 'QCmcs_Class':
        result = np.mean(resCacatoesTab['QCmcs_Class'][0:nbVal])
    elif wantedIndice == 'QCmcs_Flag':
        result = np.mean(resCacatoesTab['QCmcs_Flag'][0:nbVal])
    elif wantedIndice == 'INT_Scum':
        result = np.sum(resCacatoesTab['INT_Scum'][0:nbVal])
    elif wantedIndice == 'INT_Surf210K_at_Tmax':
        result = np.sum(resCacatoesTab['INT_Surf210K_at_Tmax'][0:nbVal])
    elif wantedIndice == 'INT_Tbavg208K_at_Tmax':
        if len(resCacatoesTab['INT_Tbavg208K_at_Tmax'])>0:
            result = np.min(resCacatoesTab['INT_Tbavg208K_at_Tmax'][0:nbVal])
            if result < 1:
                #result = 210 #valeur bidon (>208) par défaut pour ne pas avoir des 0 absolus dans le tableau de températures
                result = np.min(resCacatoesTab['INT_Tbmin'][0:nbVal])
                if result < 208.0:
                    result = 208.0
        else:
            result = 210 #valeur bidon (>208) par défaut pour ne pas avoir des 0 absolus dans le tableau de températures
    elif wantedIndice == 'INT_Ecc220K_at_Tmax':
        result = np.mean(resCacatoesTab['INT_Ecc220K_at_Tmax'][0:nbVal])
    elif wantedIndice == 'INT_orientation220K_at_Tmax':
        result = np.mean(resCacatoesTab['INT_orientation220K_at_Tmax'][0:nbVal])
    elif wantedIndice == 'Temp brightness mini':
        if len(resCacatoesTab['INT_Tbmin'])>0:
            result = np.min(resCacatoesTab['INT_Tbmin'])
        else:
            result = 200 #valeur bidon par défaut pour ne pas avoir des 0 absolus dans le tableau de températures
    elif wantedIndice == 'NB MCS':
        result = resCacatoesTab['DAILYmcs_Pop']
    elif wantedIndice == 'Fraction MCS 210K x durée dans maille':   
        startInGrid = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        endInGrid = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        prevSahelDay = resCacatoesTab['PREV_SAHEL_DAY'][0:nbVal]
        realGridTime = getRealGridTime(startInGrid,endInGrid, prevSahelDay, 24, needCheckVeille)
        valueIST = 0.01 * resCacatoesTab['INT_Sfract_210K'][0:nbVal] * resCacatoesTab['INT_Smax_210K'][0:nbVal] * realGridTime
        result = np.sum(valueIST)
    elif wantedIndice == 'Durée sur cellule':
        result = np.sum(resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]-resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal])
    elif wantedIndice == 'Distance à Tmax index' or wantedIndice == 'Durée sur cellule autour Tmax':
        distToMax = 0
        indiceduree = 0
        #print('day=',day)
        for j in np.arange(nbVal):
            gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][j]
            gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][j]
            initMCS = resCacatoesTab['INIT_Time'][j]
            endMCS = resCacatoesTab['END_Time'][j]
            TmaxMCS = resCacatoesTab['INT_Tmax'][j]
            prevSahelDay = resCacatoesTab['PREV_SAHEL_DAY'][j]
            distTmaxMCS = getDistToTmax(day, gridStart,gridEnd, initMCS, endMCS, TmaxMCS, prevSahelDay, needCheckVeille)
            distToMax = distToMax + distTmaxMCS
            #print('initMCS=',initMCS,'endMCS=',endMCS,'TmaxMCS=',TmaxMCS,'gridStart=',gridStart,'gridEnd=',gridEnd,'distTmaxMCS=',distTmaxMCS)
            if distTmaxMCS == 0:
                indiceduree = indiceduree + (gridEnd - gridStart)
        if wantedIndice == 'Distance à Tmax index':
            result = distToMax
        else:
            result = indiceduree
    elif wantedIndice == 'Area 210K x durée avant Tmax':
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        valueArea = 0.01 * resCacatoesTab['INT_Sfract_210K'][0:nbVal] * resCacatoesTab['INT_Smax_210K'][0:nbVal]
        gridTimeBeforeTmax = getGridTimeBeforeTmax(day, gridStart,gridEnd, TmaxMCS, initMCS, endMCS)
        result = np.sum(valueArea * gridTimeBeforeTmax)
    elif wantedIndice == 'Grid fraction 210K x durée avant Tmax':
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        valueArea = 0.01 * resCacatoesTab['INT_GridFraction_210K'][0:nbVal]
        gridTimeBeforeTmax = getGridTimeBeforeTmax(day, gridStart,gridEnd, TmaxMCS, initMCS, endMCS)
        result = np.sum(valueArea * gridTimeBeforeTmax)
    elif wantedIndice == 'Durée avant Tmax':
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        gridTimeBeforeTmax = getGridTimeBeforeTmax(day, gridStart,gridEnd, TmaxMCS, initMCS, endMCS)
        result = np.sum(gridTimeBeforeTmax)
    elif wantedIndice == 'Jirak':        
        result = np.mean(resCacatoesTab['INT_CLASSIF_JIRAK'][0:nbVal])
    elif wantedIndice == 'Dist to cell at start':
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        valueArea = 0.01 * resCacatoesTab['INT_GridFraction_210K'][0:nbVal]
        gridTimeBeforeTmax = getGridTimeBeforeTmax(day, gridStart,gridEnd, TmaxMCS, initMCS, endMCS)
        lonTab = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTab = resCacatoesTab['INIT_Lat'][0:nbVal]
        lonDiff = lonTab - 2.5
        latDiff = latTab - 12.5
        distTab = np.sqrt(lonDiff*lonDiff + latDiff*latDiff)
        MSC_ToTakeTab = valueArea * gridTimeBeforeTmax
        result = 0
        for j in np.arange(nbVal):
            if MSC_ToTakeTab[j] > 0.005:
                result += distTab[j]
    elif wantedIndice == 'Dist to cell at end':
        lonTab = resCacatoesTab['END_Lon'][0:nbVal]
        latTab = resCacatoesTab['END_lat'][0:nbVal]
        lonDiff = lonTab - 2.5
        latDiff = latTab - 12.5
        distTab = np.sqrt(lonDiff*lonDiff + latDiff*latDiff)
        
        result = np.sum(distTab)
    elif wantedIndice == 'Ratio distance-S235':
        distance = resCacatoesTab['INT_Distance'][0:nbVal]
        S235 = resCacatoesTab['INT_Smax_235K'][0:nbVal]
        ratio = distance / S235
        result = np.sum(ratio)
    elif wantedIndice == 'Speed index':
        distance = resCacatoesTab['INT_Distance'][0:nbVal]
        duree = resCacatoesTab['INT_Duration'][0:nbVal]
        ratio = distance / duree
        #ratio = ratio / 3.6 #m/s
        result = np.mean(ratio)
        #print('Speed index')
        #print('Distance = ',distance, ' Duree= ', duree, ' Speed= ', result)
    elif wantedIndice == 'Distance before Tmax index':
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        lonTabStart = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTabStart = resCacatoesTab['INIT_Lat'][0:nbVal]
        distBeforeTmax = 108 * np.sqrt(np.square(lonTmax - lonTabStart) + np.square(latTmax - latTabStart))
        result = np.mean(distBeforeTmax)
    elif wantedIndice == 'Distance after Tmax index':
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        lonTabEnd = resCacatoesTab['END_Lon'][0:nbVal]
        latTabEnd = resCacatoesTab['END_lat'][0:nbVal]
        distAfterTmax = 108 * np.sqrt(np.square(lonTabEnd - lonTmax) + np.square(latTabEnd - latTmax))
        result = np.mean(distAfterTmax)
    elif wantedIndice == 'Speed before Tmax index':
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        lonTabStart = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTabStart = resCacatoesTab['INIT_Lat'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        distBeforeTmax = 108 * np.sqrt(np.square(lonTmax - lonTabStart) + np.square(latTmax - latTabStart))
        timeBeforeTmax = getMCSDurationBeforeTmax(day, TmaxMCS, initMCS, endMCS)
        
        ratio = distBeforeTmax / timeBeforeTmax
        #ratio = ratio / 3.6 #m/s
        result = np.mean(ratio)
        #print('Tmax= ',TmaxMCS,' Distance = ',distBeforeTmax, ' Duree= ', timeBeforeTmax, ' Speed= ', result, ' posInit=' , lonTabStart, latTabStart, ' posTmax=' , lonTmax, latTmax)
    elif wantedIndice == 'Speed after Tmax index':
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        lonTabEnd = resCacatoesTab['END_Lon'][0:nbVal]
        latTabEnd = resCacatoesTab['END_lat'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        distAfterTmax = 108 * np.sqrt(np.square(lonTabEnd - lonTmax) + np.square(latTabEnd - latTmax))
        timeAfterTmax = getMCSDurationAfterTmax(day, TmaxMCS, initMCS, endMCS)
        
        ratio = distAfterTmax / timeAfterTmax
        #ratio = ratio / 3.6 #m/s
        result = np.mean(ratio)
        #print('Tmax= ',TmaxMCS,' Distance = ',distAfterTmax, ' Duree= ', timeAfterTmax, ' Speed= ', result, ' posEnd=' , lonTabEnd, latTabEnd, ' posTmax=' , lonTmax, latTmax,)
    elif wantedIndice == 'Durée totale':
        duree = resCacatoesTab['INT_Duration'][0:nbVal]
        result = np.sum(duree)
    elif wantedIndice == 'Grid fraction 235K x durée totale':
        duree = resCacatoesTab['INT_Duration'][0:nbVal]
        fraction = resCacatoesTab['INT_GridFraction_235K'][0:nbVal]
        result = np.sum(0.01*duree*fraction)
    elif wantedIndice == 'Grid fraction 235K x Smax 235':
        surface = resCacatoesTab['INT_Smax_235K'][0:nbVal]
        fraction = resCacatoesTab['INT_GridFraction_235K'][0:nbVal]
        result = np.sum(0.01*fraction*surface)
    elif wantedIndice == 'Grid fraction 210K x Smax 210':
        surface = resCacatoesTab['INT_Smax_210K'][0:nbVal]
        fraction = resCacatoesTab['INT_GridFraction_210K'][0:nbVal]
        result = np.sum(fraction*surface)
    elif wantedIndice == 'Grid fraction 235K x Scum':
        surface = resCacatoesTab['INT_Scum'][0:nbVal]
        fraction = resCacatoesTab['INT_GridFraction_235K'][0:nbVal]
        result = np.sum(fraction*surface)
    elif wantedIndice == 'Duration normalized with distance before Tmax':
        index = 0
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        lonTabStart = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTabStart = resCacatoesTab['INIT_Lat'][0:nbVal]
        lonTabEnd = resCacatoesTab['END_Lon'][0:nbVal]
        latTabEnd = resCacatoesTab['END_lat'][0:nbVal]
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        S235 = resCacatoesTab['INT_Smax_235K'][0:nbVal]
        rayon = np.sqrt(S235 / np.pi) / 108
        for j in np.arange(nbVal):
            tabPointsXbefore, tabPointsYbefore = GetPointsOnPathBeforeTmax(day, gridStart[j],gridEnd[j], TmaxMCS[j], initMCS[j], endMCS[j], lonTabStart[j], latTabStart[j], lonTabEnd[j], latTabEnd[j], lonTmax[j], latTmax[j])
            distToCellTestBefore = DistanceToCell(tabPointsXbefore, tabPointsYbefore, [[2.0, 3.0], [13.0, 14.0]])
            diffToRayonBefore = np.empty(len(distToCellTestBefore))
            for i in np.arange(len(distToCellTestBefore)):       
                diffToRayonBefore[i] = np.maximum(rayon[j] - distToCellTestBefore[i] , 0)
            
            indicateurBefore = diffToRayonBefore / (rayon[j])
            
            index += np.sum(indicateurBefore)
        #print('index total =', index)
        result = index
    elif wantedIndice == 'Duration normalized with distance all life':
        index = 0
        gridStart = resCacatoesTab['INT_gridtimeOccupation_start'][0:nbVal]
        gridEnd = resCacatoesTab['INT_gridtimeOccupation_end'][0:nbVal]
        TmaxMCS = resCacatoesTab['INT_Tmax'][0:nbVal]
        initMCS = resCacatoesTab['INIT_Time'][0:nbVal]
        endMCS = resCacatoesTab['END_Time'][0:nbVal]
        lonTabStart = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTabStart = resCacatoesTab['INIT_Lat'][0:nbVal]
        lonTabEnd = resCacatoesTab['END_Lon'][0:nbVal]
        latTabEnd = resCacatoesTab['END_lat'][0:nbVal]
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        S235 = resCacatoesTab['INT_Smax_235K'][0:nbVal]
        rayon = np.sqrt(S235 / np.pi) / 108
        for j in np.arange(nbVal):
            tabPointsXbefore, tabPointsYbefore = GetPointsOnPathBeforeTmax(day, gridStart[j],gridEnd[j], TmaxMCS[j], initMCS[j], endMCS[j], lonTabStart[j], latTabStart[j], lonTabEnd[j], latTabEnd[j], lonTmax[j], latTmax[j])
            distToCellTestBefore = DistanceToCell(tabPointsXbefore, tabPointsYbefore, [[2.0, 3.0], [13.0, 14.0]])
            diffToRayonBefore = np.empty(len(distToCellTestBefore))
            for i in np.arange(len(distToCellTestBefore)):       
                diffToRayonBefore[i] = np.maximum(rayon[j] - distToCellTestBefore[i] , 0)
            
            indicateurBefore = diffToRayonBefore / (rayon[j])
            
            tabPointsXafter, tabPointsYafter = GetPointsOnPathAfterTmax(day, gridStart[j],gridEnd[j], TmaxMCS[j], initMCS[j], endMCS[j], lonTabStart[j], latTabStart[j], lonTabEnd[j], latTabEnd[j], lonTmax[j], latTmax[j])
            distToCellTestAfter = DistanceToCell(tabPointsXafter, tabPointsYafter, [[2.0, 3.0], [13.0, 14.0]])
            diffToRayonAfter = np.empty(len(distToCellTestAfter))
            for i in np.arange(len(distToCellTestAfter)):       
                diffToRayonAfter[i] = np.maximum(rayon[j] - distToCellTestAfter[i] , 0)
            
            indicateurAfter = diffToRayonAfter / (rayon[j])
            
            #print('     somme indicateur before =', np.sum(indicateurBefore))
            #print('     somme indicateur after =', np.sum(indicateurAfter))
            index += np.sum(indicateurBefore)
            index += np.sum(indicateurAfter)
        #print('index total =', index)
        result = index
    elif wantedIndice == 'Dist to cell at Tmax':
        lonTabStart = resCacatoesTab['INIT_Lon'][0:nbVal]
        latTabStart = resCacatoesTab['INIT_Lat'][0:nbVal]
        lonTabEnd = resCacatoesTab['END_Lon'][0:nbVal]
        latTabEnd = resCacatoesTab['END_lat'][0:nbVal]
        lonTmax = resCacatoesTab['INT_Lon_at_Tmax'][0:nbVal]
        latTmax = resCacatoesTab['INT_Lat_at_Tmax'][0:nbVal]
        #print('lonStart=',lonTabStart[0],'latStart=',latTabStart[0])
        #print('lonTmax=',lonTmax[0],'latTmax=',latTmax[0])
        #print('lonEnd=',lonTabEnd[0],'latEnd=',latTabEnd[0])
        #S235 = resCacatoesTab['INT_Smax_235K'][0:nbVal]
        #rayon = np.sqrt(S235 / np.pi) / 108
        #print('rayon=',rayon[0])
        #orientation = resCacatoesTab['INT_orientation220K_at_Tmax'][0:nbVal]
        #angle = (orientation + 180) * np.pi / 180
        #print('orientation=',orientation[0])
        ratio = 0.75
        distToCell = 0.0
        
        for j in np.arange(nbVal):
            xToTake = lonTmax[j]
            yToTake = latTmax[j]
            #print('point testé=',xToTake, yToTake)
            lonDiff1 = np.maximum(xToTake - 3,0)
            lonDiff2 = np.maximum(2 - xToTake,0)
            latDiff1 = np.maximum(yToTake - 14,0)
            latDiff2 = np.maximum(13 - yToTake,0)
            distToCell += (lonDiff1 + lonDiff2 + latDiff1 + latDiff2)
        
        result = distToCell
    else:
        raise Exception("La variable :" + wantedIndice + "ne figure pas dans la liste des indices !")
    return result
def DistanceToCell(tabPointsX, tabPointsY, cell):
    nbPoints = len(tabPointsX)
    distTab = np.empty(nbPoints)
    lonmin = cell[0][0]
    lonmax = cell[0][1]
    latmin = cell[1][0]
    latmax = cell[1][1]
    for i in np.arange(nbPoints):
        x = tabPointsX[i]
        y = tabPointsY[i]
        lonDiff1 = np.maximum(x - lonmax,0)
        lonDiff2 = np.maximum(lonmin - x,0)
        latDiff1 = np.maximum(y - latmax,0)
        latDiff2 = np.maximum(latmin - y,0)
        distTab[i] = (lonDiff1 + lonDiff2 + latDiff1 + latDiff2)
    return distTab
   
def getAbsoluteTmax(day, initMCS, endMCS, TmaxMCS):
    MCSduration = endMCS - initMCS
    tmax = initMCS + 0.01 * TmaxMCS * MCSduration
    timestamp = tmax *24*3600
    tMaxInPandaFormat = np.empty(len(timestamp),dtype=object)
    for t in np.arange(len(timestamp)):
        tMaxInPandaFormat[t] = pd.Timestamp(timestamp[t], unit='s')
        t += 1
    return tMaxInPandaFormat
    
    

    
