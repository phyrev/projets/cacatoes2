# Répertoire Jupyter Lab notebooks

Permet d'utiliser les scripts et de visualiser les résultats

## AMMA-Catch_ClimatoMCS_Only.ipynb

- **Climatologie des MCS sur les données de pluie de la maille AMMA-Catch Niger**
    - Run_MCS_Climatology.GetAllMCSDataFromCacatoesAndAmmaCatch(1990, 2016, niger_cell, 0.5, wantedIndice,'', '../CACATOES/AMMA-CATCH/')
    - Run_MCS_Climatology.PlotMCSClimatology(t1, t2, wantedIndice, [85,90,99.5])


## TendancePluieBadoplu_Only.ipynb

- **Tendance "simple" sur les pluies (données badoplu au Sahel)**
    - dfPluie = Run_Badoplu_Tendance.GetAllRainDataFromBadoplu(1989, 2015, [[-50,50],[10,30]], '')
    - Run_Badoplu_Tendance.PlotRainTendance(dfPluie, 3, 2.0)

## TendanceMCS_only.ipynb

- **Tendance sur les MCS**
    - dfGlobal = Run_MCS_Tendance.GetAllMCSDataFromCacatoes(1989, 2015, 6, 9, [[-2,9],[10,15]],variablesDictionary)
    - Run_MCS_Tendance.PlotNbMCSTendance(dfGlobal, 3)
    - Run_MCS_Tendance.PlotNbMCSVariablesTendance(dfGlobal, 3,variablesDictionary, 99)

## ViewSahelStations.ipynb

Notebook avec une IHM ipyleaflet permettant de :

- visualiser les différentes stations Badoplu
- sélectionner un sous ensemble de stations avec l'outil graphique "rectangle"
- choisir une période d'analyse
- définir un seuil de pluvio
- calculer les valeurs des indices "Cacatoes" sélectionnés pour les stations et les jours correspondant (bouton "GO")
- repartir à zéros avec le bouton "ClearDatas"

- Les résultats obtenus sont plottés :
    - Un graphe par indice

- La dernière cellule du notebook permet de visualiser une tendance des pluies extrêmes avec une loi de gumbel simple sur les stations sélectionnées uniquement !



